<?php echo '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  @foreach($links as $link)
  <url>
    <loc>https://fiyat360.com/category/{!! $link->variant_id !!}</loc>
    <lastmod><?php echo date('Y-m-d'); ?></lastmod>
    <changefreq>daily</changefreq>
  </url>
  @endforeach
</urlset>
