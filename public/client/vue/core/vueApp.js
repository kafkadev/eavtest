if (['www.fiyat360.com', 'fiyat360.com'].includes(location.hostname)) {
    window.apiUrl = '//api.fiyat360.com/api';
} else {
    window.apiUrl = location.origin.replace('test.', '') + '/api';
}


var App = httpVueLoader('./vue/container/app.vue');
var Blank = httpVueLoader('./vue/page/blank.vue');
var Category = httpVueLoader('./vue/page/category.vue');
var Single = httpVueLoader('./vue/page/single.vue');
var Login = httpVueLoader('./vue/page/login.vue');
var TestSingle = httpVueLoader('./vue/page/test-single.vue');
var MyAccount = httpVueLoader('./vue/page/myaccount.vue');
var Product = httpVueLoader('./vue/page/product.vue');
var Compare = httpVueLoader('./vue/page/compare.vue');
var CompareItem = httpVueLoader('./vue/page/compareItem.vue');
var Classified = httpVueLoader('./vue/page/classified.vue');

var HeaderSort = httpVueLoader('./vue/components/theme/header-sort.vue')
var SiteHeader = httpVueLoader('./vue/components/theme/header.vue')
var SiteFoter = httpVueLoader('./vue/components/theme/footer.vue')

//PARTS
var SingleGallery = httpVueLoader('./vue/components/single-gallery.vue')
var SingleGallery2 = httpVueLoader('./vue/components/single-gallery2.vue')
var SingleRightDetail = httpVueLoader('./vue/components/single-right-detail.vue')
var SingleRightDetail2 = httpVueLoader('./vue/components/single-right-detail2.vue')
var PricesTable = httpVueLoader('./vue/components/prices-table.vue')
var SingleReviewsTable = httpVueLoader('./vue/components/single-reviews-table.vue')
var SingleSpecsTable = httpVueLoader('./vue/components/single-specs-table.vue')
var SingleSpecsTable2 = httpVueLoader('./vue/components/single-specs-table2.vue')
var SingleDescTable = httpVueLoader('./vue/components/single-desc-table.vue')
var SingleBreadcrumb = httpVueLoader('./vue/components/single-breadcrumb.vue')
var CategoryList1 = httpVueLoader('./vue/components/category-list1.vue')
Vue.mixin({
    components: {
        HeaderSort,
        SiteHeader,
        SiteFoter,
        SingleGallery,
        SingleGallery2,
        SingleRightDetail,
        SingleRightDetail2,
        PricesTable,
        SingleReviewsTable,
        SingleSpecsTable,
        SingleDescTable,
        SingleBreadcrumb,
        SingleSpecsTable2,
        CategoryList1
    },
    data() {
        return {
            clientInit: {},
            mainCategories: [],
            userData: {}
        };
    },
    computed: {
        compTest() {
            return 1;
        }
    },
    methods: {
        currencyFormat(num, currency = ' ₺') {
            console.log(num);
            console.log(num.toLocaleString('tr-TR', {
                style: 'currency',
                currency: 'TRY'
            }));

            //https://flaviocopes.com/how-to-format-number-as-currency-javascript/
            return (
                num
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + currency
            ) // use . as a separator
        },
        getUserData() {
            if (this.userData.api_token) {

                return this.userData
            } else {
                this.userData = localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')) : {}
                return this.userData
            }

        },
        logoutPanel() {
            localStorage.removeItem('api_token');
            localStorage.removeItem('userData');
            window.location.reload(1)
        },

        checkRole(roles = [], perm = 0) {
            if (roles.includes(this.$root.userData.role)) {
                return true;
            } else {
                return false
            }
        },
        checkLogin() {
            let data = this.getUserData()
            if (data.api_token) {
                return true;
            } else {
                return false;
            }
        },
        getLast() {
            this.get('//' + apiUrl + '/last10', (response) => {});
        },
        post(url, pdata, cb) {
            fetch(url, this.postHeader(pdata))
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                cb(data);
            });
        },
        get(url, cb) {
            fetch(url, this.getHeader())
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                cb(data);
            });
        },
        getHeader() {
            return {
                credentials: 'omit',
                method: 'GET',
                headers: {
                    //"Authorization": this.getToken(),
                    Accept: 'application/json, application/xml, text/plain, text/html, *.*'
                }
            };
        },
        postHeader(data = {}) {
            return {
                headers: {
                    //"Authorization": this.getToken(),
                    "Content-Type": "application/json",
                    Accept: 'application/json, application/xml, text/plain, text/html, *.*'
                },
                credentials: 'omit',
                method: 'POST',
                body: data
            };
        },
        getToken() {
            return 'Bearer ' + JSON.parse(localStorage.getItem('api_token'));
        }
    }
});

var routes = [{
    path: '/',
    name: 'Blank',
    component: Blank
},
{
    path: '/category/:catId?',
    name: 'category',
    props: true,
    component: Category
},
{
    path: '/login',
    name: 'Login',
    props: true,
    component: Login
},
{
    path: '/myaccount',
    name: 'MyAccount',
    props: true,
    component: MyAccount,
    beforeEnter: (to, from, next) => {
        
        if (localStorage.getItem('userData')) {
            next();
        } else {
            next({name:'Blank'})
        }
      }
},
{
    path: '/compare/:itemId?',
    name: 'Compare',
    component: Compare
},
{
    path: '/compare-item/:item1?_:item2?/:slug.html',
    name: 'CompareItem',
    component: CompareItem,
    props: true
},
{
    path: '/titem/:itemId?',
    name: 'TestSingle',
    component: TestSingle
},
{
    path: '/product/*_:itemId.html',
    name: 'Product',
    component: Product
},
{
    path: '/classified/:itemId?',
    name: 'Classified',
    component: Classified
}
];

var router = new VueRouter({
    mode: 'history',
    routes: routes,
    scrollBehavior(to, from) {
        return {
            x: 0,
            y: 0
        };
    }
});


//https://markus.oberlehner.net/blog/implementing-a-simple-middleware-with-vue-router/

router.beforeEach((to, from, next) => {
    next();
});

router.afterEach((to, from) => {});


var vueApp = new Vue({
    el: "#app",
    router: router,
    template: "<App/>",
    data() {
        return {};
    },
    mixins: [],
    components: {
        App
    },
    beforeCreate() {},
    created() {
        this.userData = this.getUserData()
        this.get(apiUrl + '/clientInit', (res) => {
            this.$root.clientInit = res
        });


    },
    mounted() {

    },
    methods: {}
});
