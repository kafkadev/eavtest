'use strict';
var Main = httpVueLoader('./vue/App.vue')
var User = httpVueLoader('./vue/user.vue')
var Sidebar = httpVueLoader('./vue/sidebar.vue')
var Post = httpVueLoader('./vue/posts.vue')
var AddPost = httpVueLoader('./vue/add-posts.vue')
var Entry = httpVueLoader('./vue/entry.vue')
var Entity = httpVueLoader('./vue/entity.vue')
var Entries = httpVueLoader('./vue/entries.vue')
var Entities = httpVueLoader('./vue/entities.vue')
var Home = httpVueLoader('./vue/home.vue')
var Brand = httpVueLoader('./vue/brand.vue')
var Category = httpVueLoader('./vue/category.vue')
var Categories = httpVueLoader('./vue/categories.vue')
var UserList = httpVueLoader('./vue/user-list.vue')
var Login = httpVueLoader('./vue/login.vue')
var Erken = httpVueLoader('./vue/erken.vue')
var NamedWrapper = httpVueLoader('./vue/named.vue')



/* Router and App setup: */
var routes = [
{
  path: '/login',
  name: 'Login',
  component: Login
},
{
  path: '/content/add',
  name: 'addposts',
  props: true,
  component: AddPost
},
{
  path: '/post/:post_id',
  name: 'posts',
  props: true,
  component: Post
},
{
  path: '/entry/:entry_id',
  name: 'entry',
  props: true,
  component: Entry
},
{
  path: '/entries/:entity_type',
  name: 'entries',
  props: true,
  component: Entries
},
{
  path: '/entities/:entity_type',
  name: 'entities',
  props: true,
  component: Entities
},
{
  path: '/entity/:entity_type',
  name: 'entity',
  props: true,
  component: Entity
},
{
  path: '/categories',
  name: 'categories',
  props: true,
  component: Categories
},
{
  path: '/',
  name: 'home',
  props: true,
  component: Home
}
];

var router = new VueRouter({
  //mode: 'history',
  routes: routes,
  scrollBehavior(to, from) {
        /*if (savedPosition) {
          return savedPosition
        } else {
          return { x: 0, y: 0 }
        }*/
        return {
          x: 0,
          y: 0
        }
      }
    });






router.beforeEach((to, from, next) => {
  console.log(Date.now())

  next();
});

router.afterEach((to, from) => {
  console.log(Date.now())
});

var app = new Vue({
  router: router,
  el: '#app',
  render: h => h(Main),
  template: '<Main/>',
  components: {
   Main,
   'sidebar-marka': Sidebar
 },
 data() {
  return {

  }
},
methods: {
  linkGo(path) {
    this.$router.push(path);
  }
}
});
