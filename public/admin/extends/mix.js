'use strict';
/*localforage.config({
    driver: localforage.INDEXEDDB,
    name: 'bet_manage',
    version: 1.0,
    storeName: 'bet_db',
    description: 'general db'
});

localStorage.setItem("storageIsOpen", true);
*/
            Array.prototype.move = function(from,to){
  this.splice(to,0,this.splice(from,1)[0]);
  return this;
};

var AttributeList = httpVueLoader('./vue/components/eav/attribute-list.vue');
var AttributeSelectModal = httpVueLoader('./vue/components/eav/attribute-select-modal.vue');
var GroupEditModal = httpVueLoader('./vue/components/eav/group-edit-modal.vue');
var FromGroupModal = httpVueLoader('./vue/components/eav/form-group-modal.vue');
Vue.mixin({
    components: {
        AttributeList,
        AttributeSelectModal,
        GroupEditModal,
        FromGroupModal
    },
    data() {
        return {
            entities: {},
            allMatches: {},
            localGames: {},
            allCounts: {},
            marketOdds: {},
            langHelper: {},
            currentInsertTypeForm: {},
            api: {
                api_url: '//rakipbahis01.com'
            },
            sistemList: {}
        }
    },
    computed: {
        preMatches() {
            return true;
        }
    },
    methods: {
        formData(file, cb) {
            const formData = new FormData()
            formData.append('filem', file)
            axios.post({
                method: 'post',
                url: dState.api + '/my/documents',
                data: file
            });
            //const formData = new FormData()
            // var demosa = formData.append('filem', file)
            //console.log(fileHeader(demosa))
            /*
              fetch(dState.api + '/my/documents', fileHeader(file))
              .then(res => {
                console.log(res)
              })
              */
        },
        fileHeader(data) {
            return {
                method: "POST",
                headers: {
                    Authorization: "Bearer " + JSON.parse(localStorage.getItem("api_token")),
                    Accept: "application/json, application/xml, text/plain, text/html, *.*"
                },
                body: {
                    filem: data
                }
            }
        },
        slugify(str, sep = '_') {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆĞÍÌÎÏİŇÑÓÖÒÔÕØŘŔŠŞŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇğíìîïıňñóöòôõøðřŕšşťúůüùûýÿžþÞĐđßÆa·/-,:;";
            var to = "AAAAAACCCDEEEEEEEEGIIIIINNOOOOOORRSSTUUUUUYYZaaaaaacccdeeeeeeeegiiiiinnooooooorrsstuuuuuyyzbBDdBAa--_---";
            for (var i = 0, l = from.length; i < l; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 _]/g, '') // remove invalid chars
                .replace(/\s+/g, sep) // collapse whitespace and replace by -
                .replace(/-+/g, sep); // collapse dashes

            return str;
        },
        checkLogin() {
            let data = localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')) : {}
            if (data.api_token) {
                return true;
             } else {
                 return false;
             }
        },
        getEntityTypes() {
            this.get(window.api_url + "/categoryList3/asasa", (response) => {
               console.log(response)
            })

            this.get(window.api_url + "/entities", (response) => {
                this.entities = _.indexBy(response, 'id');
            })
        },
        getTermsField() {
            this.get(window.api_url + "/terms", (response) => {
                this.taxonomyTerms = _.groupBy(response, 'entity_type_code')
            })
        },
        /* LOCAL DATA METHODS */
        setLocalDataWithKey(key_name = '/test', data = {}) {
            return localStorage.setItem(key_name, JSON.stringify(data));
        },
        getLocalDataWithKey(key_name = '/test') {
            return JSON.parse(localStorage.getItem(key_name));
        },
        getMockDataWithKey(key_name = 'Games_All', callBack) {
            /*
            use like: this.$root.getMockDataWithKey('Games_All', (res => console.log(res)))
            */
            this.get('/json/test01.json', (response) => {
                callBack(response[key_name]);
            })
        },
        getMockDataAsFull(callBack) {
            this.get('//rakipbahis01.com/client/test01', (response) => {
                callBack(response);
            })
        },
        getCasinoData() {
            this.get('//rakipbahis01.com/client/casino', (response) => {
                this.setLocalDataWithKey('/casino', response)
            })
        },


        /* INIT METHODS */
        setInıtData(type = 'All' /*'All,Minutes,Today'*/) {
            localforage.getItem('/asmatches', (err, matches) => {
                this.processCountBy(matches);
                this.allMatches = _.indexBy(matches, 'id');
            });

            localforage.getItem('/asmarketOdds', (err, value) => {
                let orans = _.groupBy(value, 'mac_id');
                _.each(orans, (oranlar, mac_id) => {
                    this.marketOdds[mac_id] = _.groupBy(oranlar, 'hitit_oran_id');
                })
            });

            localforage.getItem('/aslocalGameList', (err, value) => {
                this.localGames = _.indexBy(value, 'id')
            });
            localforage.getItem('/asmarkets', (err, value) => {
                this.marketList = _.indexBy(value, 'hitit_oran_id')
            });
        },


        initData(cal = 0) {

            let update_init_data_time = localStorage.getItem('update_init_data_time');
            let resTime = 0
            if (!update_init_data_time || Number(update_init_data_time) + (10 * 60 * 1000) < Date.now()) {
                this.getCasinoData();
                this.getMockDataAsFull((allData => {

                    if (allData) {
                        resTime = 1
                        let update_time = Date.now();
                        for (const key in allData) {
                            let currentData = JSON.stringify(allData[key])
                            if (key == "marketOdds" || key == "matches") { }
                            else {
                                localStorage.setItem('/as' + key, currentData);
                            }
                            if (key == "matches") {
                                allData[key] = allData[key].map((match) => {
                                    match.hitit_tarih_baslangic = match.hitit_tarih_baslangic.replace(/-/g, "/");
                                    return match;
                                });
                                //localStorage.setItem('/as' + key, JSON.stringify(_.groupBy(allData[key], 'mac_id')));
                            }
                            localforage.setItem('/as' + key, allData[key], (err) => { });
                        }

                        localStorage.setItem('update_init_data_time', JSON.stringify(update_time));
                    }
                    setTimeout(() => {

                        this.setInıtData();
                    }, 600);
                }))
            } else {
                if (!resTime) {
                    resTime = 1
                    this.setInıtData();
                }
            }

            setTimeout(() => {
                if (!resTime) {

                    this.setInıtData();
                }
            }, 3000);

        },

        /* API AREA */
        post(url, pdata, cb) {
            fetch(url, this.postHeader(pdata)).then((response) => {
                return response.json();
            }).then((data) => {
                cb(data)
            });
        },
        get(url, cb) {
            fetch(url, this.getHeader()).then((response) => {
                return response.json();
            }).then((data) => {
                cb(data)
            });
        },
        getHeader() {
            return {
                //credentials: 'include',
                //mode: 'no-cors',
                method: "GET",
                headers: {
                    //"Authorization": this.getToken(),
                    "Accept": "application/json"
                }
            }
        },
        postHeader(data = {}) {
            return {
                headers: {
                    //"Authorization": this.getToken(),
                    "Content-Type": "application/json",
                    "Accept": "application/json, application/xml, text/plain, text/html, *.*",
                },
                //credentials: 'include',
                method: "POST",
                body: JSON.stringify(data)
            }
        },
        getToken() {
            return "Bearer " + JSON.parse(localStorage.getItem("api_token"));
        }
    },
    created() {
        //this.getEntityTypes()
       // this.getTermsField()
        let userData = localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')) : {}
        if (userData.api_token) {
            // this.$router.push('/')
        } else {
            // this.$router.push('/login')
        }
    },
});







/** NOTLAR */
/*
const cacheObject = {
    '/slot': (24 * 3600) * 1000,
    '/matches': (3 * 3600) * 1000,
    '/casino': (24 * 3600) * 1000,
    '/live': 60 * 1000,
    '/betongames': (24 * 3600) * 1000,
    '/tombalalive': (24 * 3600) * 1000,
    '/init': (24 * 3600) * 1000,
}

const cacheUpdate = (path) => {
    if (cacheObject[path]) {
        fetch(path, this.getHeader())
            .then(res => res.json())
            .then(data => {
                if (data) {
                    localStorage.setItem(path, Date.now())
                    //localForage.setItem(path, data)
                }
            })
    }
}

axios.defaults.baseURL = '//core.me/api';
axios.defaults.headers.common['Authorization'] = "Bearer 123456";

let macIds = _.groupBy(allOdds, 'mac_id');
let demot =  _.mapObject(macIds, (item, key, collection) => {
collection[key] = _.groupBy(item, 'hitit_oran_id')
return collection[key];

})
_.each(_.groupBy(allOdds, 'mac_id'), (oddsOfMatches, match_id) => {
    this.oranTest[match_id] = {}
    _.each(_.groupBy(oddsOfMatches, 'hitit_oran_id'), (oddsOfMatch, market_id) => {
    this.oranTest[match_id][market_id] = _.indexBy(oddsOfMatch, 'oran_id');
    });
});
localStorage.setItem('/marketOdds', JSON.stringify(this.oranTest));
*/
