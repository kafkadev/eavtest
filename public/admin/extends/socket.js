const createWebSocketPlugin = socket => {
  return store => {
    socket.onmessage = evt => {
      let socketData = JSON.parse(evt.data)
      let data = socketData.data
      let degisenler = {}
      if (socketData.type == 1) {

        store.state.degisenler = {}

        /* burada her defasında oranları bir önceki ile değiştiriyoruz */
        let disabledMatches = _.difference(store.state.disabledMatches, _.pluck(data.matches, 'id'))
        let disabledOdds = _.difference(store.state.currentOddIds, _.pluck(data.markets, 'oran_id'))
        let currentOddIds = _.pluck(data.markets, 'oran_id')

        let oldOdds = _.clone(Object.assign(store.state.oldOdds, store.state.newOdds))
        let newOdds = _.object(_.pluck(data.markets, 'oran_id'), _.pluck(data.markets, 'oran_deger'))

        /* burada oranların çıkıp indiğini hesaplıyoruz */
        _.each(newOdds, (deger, oran_id) => {
          if (oldOdds[oran_id] > deger) {
            degisenler[oran_id] = 'down'
          } else if (oldOdds[oran_id] < deger) {
            degisenler[oran_id] = 'up'
          }
        })

        /* burada maçları ve oranları id ve market türlerine göre grupluyoruz */
        store.state.LiveSports = _.countBy(data.matches, 'hitit_oyun_this_id')
        let matches = _.filter(data.matches, function(match){ 
          return !(!['3','4'].includes(match.hitit_match_durum) && disabledMatches.includes(match.id)); 
        });


        matches = _.indexBy(_.sortBy(matches, 'livebet_minutes').reverse(), 'id');
        let orans = _.groupBy(data.markets, 'mac_id');

        _.each(orans, (oranlar, mac_id) => {
          orans[mac_id] = _.groupBy(oranlar, 'hitit_oran_id');
        });


        /* Actions İşlemleri */

        let actions = _.object(_.pluck(data.actions, 'match_id'), _.pluck(data.actions, 'values'))

        actions =  _.mapObject(actions, (val, match_id) => {
          val['team1'] = {}
          val['team2'] = {}
         if (val['corners']) {
          val['team1']['corners'] = _.indexBy(val['corners']['Team1']['Counters'],'PeriodId')[255].Value
          val['team2']['corners'] = _.indexBy(val['corners']['Team2']['Counters'],'PeriodId')[255].Value
         }
         if (val['score']) {
          val['team1']['score'] = _.indexBy(val['score']['Team1']['Counters'],'PeriodId')
          val['team2']['score'] = _.indexBy(val['score']['Team2']['Counters'],'PeriodId')
         }
         if (val['red'][0]) {
          val['team1']['red'] = _.indexBy(val['red'][0],'PeriodId')[255].Value
          val['team2']['red'] = _.indexBy(val['red'][1],'PeriodId')[255].Value
         }
         if (val['yellow'][0]) {
          val['team1']['yellow'] = _.indexBy(val['yellow'][0],'PeriodId')[255].Value
          val['team2']['yellow'] = _.indexBy(val['yellow'][1],'PeriodId')[255].Value
         }
          return _.pick(val, 'team1', 'team2');
        });

        let cloneActions = _.clone(Object.assign(store.state.liveActions, actions))



        /* burada bind olabilmesi için clone yapıyoruz */
        
        let cloneMatches = _.clone(Object.assign({}, matches))
        let cloneOrans = orans

        
        store.state.liveActions = cloneActions

        /* burada gelen maçlar ve oranlar */
        //store.state.activeLigs = cloneMatches
        store.state.liveMatches = cloneMatches
        store.state.liveMarkets = cloneOrans
        

        /* burada oranların çıkıp indiği alınacak */
        store.state.newOdds = newOdds
        store.state.oldOdds = oldOdds
        store.state.degisenler = degisenler

        /* burada bir maçın bitmiş veya oranın kapalı olduğu alınacak */
        store.state.currentOddIds = currentOddIds

        store.state.disabledOdds = disabledOdds
        store.state.disabledMatches = data.ended_matches
      }

      if (socketData.type == 2) {
        store.state.currentLiveMatchChanged = {}
        store.state.currentLiveMarkets = _.clone(Object.assign({}, _.indexBy(data.markets, 'oran_id')))
        let newDetailOdds = _.object(_.pluck(data.markets, 'oran_id'), _.pluck(data.markets, 'oran_deger'))
     
        if (data.match) {
          store.state.currentLiveMatch = _.clone(Object.assign(store.state.currentLiveMatch, data.match))
        }


        let currentMatchChanged = {}
        let oldDetailOdds = _.clone(Object.assign(store.state.currentLiveMatchOldOdds, store.state.currentLiveMatchNewOdds))
        

        _.each(newDetailOdds, (deger, oran_id) => {
          if (oldDetailOdds[oran_id] > deger) {
            currentMatchChanged[oran_id] = 'down'
          } else if (oldDetailOdds[oran_id] < deger) {
            currentMatchChanged[oran_id] = 'up'
          }
        })
        store.state.currentLiveMatchChanged = currentMatchChanged
        store.state.currentLiveMatchOldOdds = oldDetailOdds
        store.state.currentLiveMatchNewOdds = newDetailOdds
        store.state.currentLiveMatchId = 0


      }
    };

  };
};

const startSocket = createWebSocketPlugin(socket);