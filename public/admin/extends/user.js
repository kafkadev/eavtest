'use strict';
Vue.mixin({
    methods: {
        getUserDataApi(callBack) {
            this.get('//rakipbahis01.com/account/initUser', (response) => {
                this.$store.dispatch('setUserData', response)
                callBack(response);
            })
        },
        
        initUser(refresh = 0) {

            //this.fetchInıtUser()
            let update_init_user_time = localStorage.getItem('update_init_user_time');

            if (refresh || !update_init_user_time || Number(update_init_user_time) + (5 * 60 * 1000) < Date.now()) {
                this.fetchInıtUser()
            } else {
                let userData = this.getLocalDataWithKey('/userData');
                this.$store.dispatch('setUserData', userData)
            }
            let lastCoupons = this.getLocalDataWithKey('/lastCoupon');
            if (lastCoupons) {

                this.$store.dispatch('startCoupon', lastCoupons);
            }
        },
        initLang(refresh = 0) {
            let update_init_lang_time = localStorage.getItem('update_init_lang_time');
            if (refresh || !update_init_lang_time || Number(update_init_lang_time) + (15*(60 * 1000)) < Date.now()) {
                this.get('//rakipbahis01.com/client/initLang', (defaultData) => {
                    this.setLocalDataWithKey('/initLang', defaultData)
                    this.setLocalDataWithKey('update_init_lang_time', Date.now())
                    this.sportsCollection = _.indexBy(defaultData.oyunlar, 'id')
                    this.countryCollection = _.indexBy(defaultData.ulkeler, 'id')
                    this.ligCollection = _.indexBy(defaultData.ligler, 'id')
                })
            } else {
                let defaultData = this.getLocalDataWithKey('/initLang')
                this.sportsCollection = _.indexBy(defaultData.oyunlar, 'id')
                this.countryCollection = _.indexBy(defaultData.ulkeler, 'id')
                this.ligCollection = _.indexBy(defaultData.ligler, 'id')
            }
        },
        fetchInıtUser(){
            this.getUserDataApi((userData => {
                this.$store.dispatch('setUserData', userData)
                this.setLocalDataWithKey('/userData', userData)
                this.setLocalDataWithKey('update_init_user_time', Date.now())
            }))
        },
        doLogout(){
            
                var cookies = document.cookie.split("; ");
                for (var c = 0; c < cookies.length; c++) {
                    var d = window.location.hostname.split(".");
                    while (d.length > 0) {
                        var cookieBase = encodeURIComponent(cookies[c].split(";")[0].split("=")[0]) + '=; expires=Thu, 01-Jan-1970 00:00:01 GMT; domain=' + d.join('.') + ' ;path=';
                        var p = location.pathname.split('/');
                        document.cookie = cookieBase + '/';
                        while (p.length > 0) {
                            document.cookie = cookieBase + p.join('/');
                            p.pop();
                        };
                        d.shift();
                    }
                }
                setTimeout(() => {
                    window.location.replace("//rakipbahis01.com/home/login");
                }, 1000);
        }
    }
});
