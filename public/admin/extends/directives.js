Vue.directive("swiperhome", {
    bind: (el) => {
        var parent = $(el);
        setTimeout(() => {
            $(document).ready(() => {
                 new Swiper(parent, {
                      // Optional parameters
                      direction: "horizontal",
                      loop: true,
            
                      // If we need pagination
                      pagination: {
                        el: ".swiper-pagination"
                      },
            
                      // Navigation arrows
                      navigation: {
                        nextEl: ".swiper-button-next",
                        prevEl: ".swiper-button-prev"
                      },
            
                      // And if we need scrollbar
                      scrollbar: {
                        el: ".swiper-scrollbar"
                      }
                    });
                    
            });
          }, 0);
        


    }
});

Vue.directive("effectme", {
    bind: (el, binding, vnode) => {
        var $that = $(el)
        $that.click(() => {
            setTimeout(() => {
                if ($that.children('.odds').hasClass('active')) {
                    var $clone = $that.clone();
                    var speed = 400;
                    if ($that.children('.odds').hasClass('active')) {
                        $that.after($clone);
                        $clone.css({
                            position: 'absolute',
                            top: $that.offset().top,
                            left: $that.offset().left,
                            display: 'block',
                            width: '100px',
                            "z-index": 999999
                        }).animate({
                            // Fly!
                            left: $('.cart').offset().left + 50,
                            top: $('.cart').offset().top + 50,
                            width: 0,
                            height: 0
                        }, speed, () => {
                            $clone.remove();
                        });
                    }

                }
            }, 0);
        });
    }
});

Vue.directive("lignone", {
    bind: (el, binding, vnode) => {
        var $that = $(el)
       
                if (!$that.children('.event').hasClass('fw')) {
                    $that.addClass('hide')
                }
    }
});
