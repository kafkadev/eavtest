let liveState = {
    currentLiveMatchChanged: {},
    currentLiveMatchOldOdds: {},
    currentLiveMatchNewOdds: {},
    LiveSports: {},
    liveActions: {},
    currentLiveMatch: {},
    currentLiveMarkets: {},
    currentLiveMatches: {},
    liveMatches: {},
    liveMarkets: {},
    liveInfoOfMatches: {},
    degisenler: {},
    oldOdds: {},
    newOdds: {},
    disabledOdds: [],
    disabledMatches: [],
    currentOddIds: []
}


let userState = {
    kuponUyari: {
        oranUyarilari: []
    },
    userCouponIds: [],
    userCoupon: {},
    userSelf: {},
    userCollection: {},
    userData: {
        kuponlar: [],
        bilgi: {
            bakiye: 0
        }
    }
}

Vue.mixin({
    store: new Vuex.Store({
        plugins: [startSocket],
        state: {
            ...liveState,
            ...userState
        },
        mutations: {
            increment({
                state
            }) {
                state.count++
            },

            addItemToCouponCommit(state, data) {
                const oran = data.odd
                state.lastResponseAtCoupon = data.response
                if (state.userCoupon[oran.oran_id]) {
                    delete(state.userCoupon[oran.oran_id])
                    Vue.set(state, 'userCoupon', state.userCoupon);
                } else {
                    Vue.set(state.userCoupon, oran.oran_id, oran);
                    Vue.set(state.userCoupon, oran.oran_id, oran);
                }

                state.userCouponIds = _.keys(state.userCoupon);

            }

        },
        actions: {
            addCurrentLiveMatch({
                commit,
                state
            }, data) {
                state.currentLiveMatch = data.match
                state.currentLiveMatchOldOdds = _.object(_.pluck(data.markets, 'oran_id'), _.pluck(data.markets, 'oran_deger'))
                state.currentLiveMarkets = _.indexBy(data.markets, 'oran_id');
            },
            addItemToCoupon({
                commit,
                state
            }, data) {
                const oran = data.odd
                state.lastResponseAtCoupon = data.response
                if (state.userCoupon[oran.mac_id] && state.userCoupon[oran.mac_id].oran_id == oran.oran_id) {
                    delete(state.userCoupon[oran.mac_id])
                    Vue.set(state, 'userCoupon', state.userCoupon);
                } else {
                    Vue.set(state.userCoupon, oran.mac_id, oran);
                }
                localStorage.setItem('/lastCoupon', JSON.stringify(state.userCoupon));
                state.userCouponIds = _.pluck(state.userCoupon, 'oran_id');
            },
            addItemToCouponOld({
                commit,
                state
            }, data) {
                const oran = data.odd
                state.lastResponseAtCoupon = data.response
                if (state.userCoupon[oran.oran_id]) {
                    delete(state.userCoupon[oran.oran_id])
                    Vue.set(state, 'userCoupon', state.userCoupon);
                } else {
                    Vue.set(state.userCoupon, oran.oran_id, oran);
                }
                state.userCouponIds = _.keys(state.userCoupon);
            },
            clearCoupon({
                commit,
                state
            }, data) {
                state.userCoupon = {}
                state.userCouponIds = []
                localStorage.setItem('/lastCoupon', JSON.stringify({}))
            },
            setUserData({
                commit,
                state
            }, data) {
                state.userData = data
            },
            startCoupon({
                commit,
                state
            }, data) {
                state.userCouponIds = _.pluck(data, 'oran_id');
                Vue.set(state, 'userCoupon', data);
            },
            kuponUyari({
                commit,
                state
            }, data) {
                state.kuponUyari = data
            },
            mergeUserLimit({
                commit,
                state
            }, data) {
                
                let oldLimit = state.userData.bilgi
                state.userData.bilgi = Object.assign(oldLimit, data)
            }
        }
    })
});