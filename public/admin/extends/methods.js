var PROTOCOL = window.location.protocol === "https:" ? "wss://" : "wss://";
var betradar_socket = "rakip01-socket.herokuapp.com"
var heroku_socket = "rakip01-socket.herokuapp.com"
var local_socket = 'localhost:3000'
var SOCKET_URL = PROTOCOL + heroku_socket;
const socket = new WebSocket(SOCKET_URL);

Vue.mixin({
    methods: {
        /* Coupon Methods */

        processAddCouponApi(callBack) {
            this.get('//rakipbahis01.com/client/confirm_bet', (response) => {
                callBack(response);
            })
        },
        processCheckAddCouponApi(callBack) {
            this.get('//rakipbahis01.com/client/confirm_bet/1', (response) => {
                callBack(response);
            })
        },
        addToCouponApi(odd, callBack) {
            this.get('//rakipbahis01.com/client/coupon/' + odd.oran_id + '/' + odd.mac_id, (response) => {
                callBack(response);
            })
        },
        listenMatchId(id) {
            socket.send(JSON.stringify({type : 2, data : Number(id)}));
        },

        /* MATCH METHODS */
        matchGo(matchId) {
            let startTime = Date.now();
            this.getMatch(matchId, (res) => {
                this.$router.push({
                    name: 'Match',
                    params: {
                        matchId: matchId,
                        purematch: res,
                        time: startTime
                    }
                });
            })
        },

        getMatch(match_id = 0, callBack) {
            this.get('//rakipbahis01.com/client/getMatch/' + match_id, (response) => {
                callBack(response);
            })
        },


        /* ODDS METHODS */
        getFullOddsByMatch(match_id = 0, callBack) {

            this.get('//aresbet.net/app/oddspop/' + match_id, (response) => {
                callBack(response);
            })

        },

        /* FILTER METHODS */
        preCustomFilterByMinutes(matchCollection = [], max_minutes = 90) {

            let asMatchesMinutes = _.filter(matchCollection,(match)=> {
                let start_minutes_date = new Date(match.hitit_tarih_baslangic);
                let start_minutes_day = start_minutes_date.getDate();
                let stop_minutes_day = new Date().getDate();
                return start_minutes_day == stop_minutes_day && start_minutes_date.getTime() >= Date.now() + (max_minutes * 60 * 1000);
            })
            return asMatchesMinutes
        },
        preCustomFilterByMinutes2(matchCollection = [], minutes = 30) {

            return _.filter(matchCollection, (match) => {
                let start_date = new Date(match.hitit_tarih_baslangic).getTime();
                return (start_date - (5 * 60 * 1000) >= Date.now()) && (start_date <= Date.now() + (minutes * 60 * 1000));
            })
        },
        preCustomFilterByDays(matchCollection = []) {
            let asMatchesDays = _.filter(matchCollection,(match)=> {
                let start_date = new Date(match.hitit_tarih_baslangic);
                let start_day = start_date.getDate();
                let stop_day = new Date().getDate();
                return start_day == stop_day && start_date.getTime() >= Date.now();
            })
            return asMatchesDays
        },
        processCountBy(matches = []) {
           /* _.each(_.countBy(matches, 'hitit_oyun_this_id'), (oyun, oyun_key) => {
                this.allCounts[oyun_key] = {
                    "count": _.size(oyun)
                };
                _.each(_.groupBy(oyun, 'hitit_ulke_this_id'), (ulke, ulke_key) => {
                    this.allCounts[oyun_key][ulke_key] = {
                        "count": _.size(ulke)
                    };
                    _.each(_.groupBy(ulke, 'hitit_lig_this_id'), (lig, lig_key) => {
                        this.allCounts[oyun_key][ulke_key][lig_key] = {
                            "count": _.size(lig)
                        };
                    });
                });
            });*/

            this.allCounts = _.countBy(matches, 'hitit_oyun_this_id')
            localStorage.setItem('/asCountsAll', JSON.stringify(this.allCounts))
        }

    }
});
