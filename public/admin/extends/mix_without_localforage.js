'use strict';
var Debug = httpVueLoader('/js/parts/debug.vue');
var FilterIcons = httpVueLoader('/js/parts/sports-icons.vue');
var Suggestions = httpVueLoader('/js/parts/suggestions.vue');
var CustomMessage = httpVueLoader('/js/parts/custom-message.vue');
var DefaultMarket = httpVueLoader('/js/parts/default-market.vue');
var HomeSlider = httpVueLoader('/js/parts/swiper.vue');
var Markets = httpVueLoader('/js/components/markets.vue');
var PreMarketsDetail = httpVueLoader('/js/components/pre-markets-detail.vue');
var LiveMarketsDetail = httpVueLoader('/js/components/live-markets-detail.vue');
var MatchsFilter = httpVueLoader('/js/components/matchs-filter.vue');
var LiveMatches = httpVueLoader('/js/components/live-matches.vue');
var LiveCoupons = httpVueLoader('/js/components/live-coupons.vue');
var LiveLastMinute = httpVueLoader('/js/components/live-last-minute.vue');
var SportsFilter = httpVueLoader('/js/components/sports-filter.vue');
var CountriesFilter = httpVueLoader('/js/components/countries-filter.vue');
var LiveDetailHeader = httpVueLoader('/js/components/live-detail-header.vue');
var LiveDetailScoreInfo = httpVueLoader('/js/components/live-detail-score-info.vue');
var PreDetailHeader = httpVueLoader('/js/components/pre-detail-header.vue');
var CouponDetail = httpVueLoader("/js/account/part-coupon-history-item.vue");


localStorage.setItem("storageIsOpen", true);


Vue.mixin({
    components: {
        FilterIcons,
        Suggestions,
        CustomMessage,
        MatchsFilter,
        LiveMatches,
        SportsFilter,
        CountriesFilter,
        Markets,
        PreMarketsDetail,
        LiveMarketsDetail,
        CouponDetail,
        Debug,
        DefaultMarket,
        LiveCoupons,
        LiveLastMinute,
        HomeSlider,
        LiveDetailHeader,
        PreDetailHeader,
        LiveDetailScoreInfo
    },
    data() {
        return {
            allMatches: {},
            fullData: {},
            localGames: {},
            allCounts: {},
            marketOdds: {},
            langHelper: {
                CustomerInfo: "Kullanıcı Bilgileri",
                MyCoupon: "Kupon",
                MyBets: "Bahislerim",
                Casino: "Casino",
                Today: "Bugün",
                LastMinute: "Son Dakika",
                Match: "Bahis",
                Sports: "Sporlar",
                Home: "Menü",
                Live: "Canlı Bahisler"
            },
            api: {
                api_url: '//rakipbahis01.com'
            },
            sistemList: {
                "3": {
                    "2": 6
                },
                "4": {
                    "2": 6,
                    "3": 4
                },
                "5": {
                    "2": 10,
                    "3": 10,
                    "4": 5
                },
                "6": {
                    "2": 15,
                    "3": 20,
                    "4": 15,
                    "5": 6
                },
                "7": {
                    "2": 21,
                    "3": 35,
                    "4": 35,
                    "5": 21,
                    "6": 7
                },
                "8": {
                    "2": 28,
                    "3": 56,
                    "4": 70,
                    "5": 56,
                    "6": 28,
                    "7": 8
                }
            }
        }
    },
    computed: {
        preMatches() {
            return _.filter(this.allMatches, (match) => {
                var start_date = new Date(match.hitit_tarih_baslangic).getTime();
                return start_date >= Date.now();
            });
        }
    },
    methods: {
        scoreComputed(scores, sport_id) {
            var res = '';
            if (scores && [4].includes(Number(sport_id)) && scores[1]) {
                res = scores[1].Value + ' | ';
            }
            if (scores && [7].includes(Number(sport_id))) {
                res = _.pluck(_.omit(scores, 255, 254), 'Value').join(' ') + ' | ';
            }
            return res;
        },
        /* LOCAL DATA METHODS */
        setLocalDataWithKey(key_name = '/test', data = {}) {
            return localStorage.setItem(key_name, JSON.stringify(data));
        },
        getLocalDataWithKey(key_name = '/test') {
            return JSON.parse(localStorage.getItem(key_name));
        },
        getMockDataWithKey(key_name = 'Games_All', callBack) {
            /*
            use like: this.$root.getMockDataWithKey('Games_All', (res => console.log(res)))
            */
            this.get('/json/test01.json', (response) => {
                callBack(response[key_name]);
            })
        },
        getMockDataAsFull(callBack) {
            this.get('//rakipbahis01.com/client/test01', (response) => {
                callBack(response);
            })
        },
        getCasinoData() {
            this.get('//rakipbahis01.com/client/casino', (response) => {
                this.setLocalDataWithKey('/casino', response)
            })
        },


        /* INIT METHODS */
        /*
        setInıtData(type = 'All' ) {
            localforage.getItem('/asmatches', (err, matches) => {
                this.processCountBy(matches);
                this.allMatches = _.indexBy(matches, 'id');
            });

            localforage.getItem('/asmarketOdds', (err, value) => {
                var orans = _.groupBy(value, 'mac_id');
                _.each(orans, (oranlar, mac_id) => {
                    this.marketOdds[mac_id] = _.groupBy(oranlar, 'hitit_oran_id');
                })
            });

            localforage.getItem('/aslocalGameList', (err, value) => {
                this.localGames = _.indexBy(value, 'id')
            });
            localforage.getItem('/asmarkets', (err, value) => {
                this.marketList = _.indexBy(value, 'hitit_oran_id')
            });
        },


        initData(cal = 0) {

            var update_init_data_time = localStorage.getItem('update_init_data_time');
            var resTime = 0
            if (!update_init_data_time || Number(update_init_data_time) + (10 * 60 * 1000) < Date.now()) {
                this.getCasinoData();
                this.getMockDataAsFull((allData => {

                    if (allData) {
                        resTime = 1
                        var update_time = Date.now();
                        for (const key in allData) {
                            var currentData = JSON.stringify(allData[key])
                              if(key == "marketOdds" || key == "matches"){}
                             else {
                                localStorage.setItem('/as' + key, currentData);
                            }
                            if (key == "matches") {
                                allData[key] = allData[key].map((match) => {
                                    match.hitit_tarih_baslangic = match.hitit_tarih_baslangic.replace(/-/g, "/");
                                    return match;
                                });
                                //localStorage.setItem('/as' + key, JSON.stringify(_.groupBy(allData[key], 'mac_id')));
                            }
                            localforage.setItem('/as' + key, allData[key], (err) => {});
                        }

                        localStorage.setItem('update_init_data_time', JSON.stringify(update_time));
                    }
                    setTimeout(() => {
                        
                        this.setInıtData();
                    }, 600);
                }))
            } else {
                if (!resTime) {
                    resTime = 1
                    this.setInıtData();
                }
            }

            setTimeout(() => {
                if (!resTime) {
                    
                    this.setInıtData();
                }
            }, 3000);

        },*/
        /* INIT METHODS */
        setInıtData(type = 'All' /*'All,Minutes,Today'*/ ) {
       
                let matches = this.fullData.asmatches

                this.allMatches = _.indexBy(matches, 'id');
                this.processCountBy(matches);

                let marketodds = this.fullData['asmarketOdds'];
                let orans = _.groupBy(marketodds, 'mac_id');
                _.each(orans, (oranlar, mac_id) => {
                    this.marketOdds[mac_id] = _.groupBy(oranlar, 'hitit_oran_id');
                })



                this.localGames = _.indexBy(this.fullData['aslocalGameList'], 'id')
                this.marketList = _.indexBy(this.fullData['asmarkets'], 'hitit_oran_id')

            let update_init_data_time = localStorage.getItem('update_init_data_time');
            if (!update_init_data_time || Number(update_init_data_time) + (15 * 60 * 1000) < Date.now()) {
                this.initData();
            }
        },


        initData(cal = 0) {


            this.getCasinoData();
            this.getMockDataAsFull((allData => {

                if (allData) {
                    let update_time = Date.now();
                    for (const key in allData) {
                        if (key == "matches") {
                            allData[key] = allData[key].map((match) => {
                                match.hitit_tarih_baslangic = match.hitit_tarih_baslangic.replace(/-/g, "/");
                                return match;
                            });
                            localStorage.setItem('/as' + key, JSON.stringify(allData[key], 'mac_id'));
                        }
                        
                        var currentData = JSON.stringify(allData[key])
                        if(key == "marketOdds" || key == "matches"){}
                        else {
                            localStorage.setItem('/as' + key, currentData);
                        }
                        this.fullData['as' + key] = allData[key];
                        
                    }

                    localStorage.setItem('update_init_data_time', JSON.stringify(update_time));
                }
                setTimeout(() => {
console.log(this.fullData);

                    this.setInıtData();
                }, 1000);
            }))
        },



        /* API AREA */
        post(url, pdata, cb) {
            fetch(url, this.postHeader(pdata)).then((response) => {
                return response.json();
            }).then((data) => {
                cb(data)
            });
        },
        get(url, cb) {
            fetch(url, this.getHeader()).then((response) => {
                return response.json();
            }).then((data) => {
                cb(data)
            });
        },
        getHeader() {
            return {
                credentials: 'include',
                method: "GET",
                headers: {
                    //"Authorization": this.getToken(),
                    "Accept": "application/json, application/xml, text/plain, text/html, *.*"
                }
            }
        },
        postHeader(data = {}) {
            return {
                headers: {
                    //"Authorization": this.getToken(),
                    "Accept": "application/json, application/xml, text/plain, text/html, *.*",
                },
                credentials: 'include',
                method: "POST",
                body: data
            }
        },
        getToken() {
            return "Bearer " + JSON.parse(localStorage.getItem("api_token"));
        }
    }
});







/** NOTLAR */
/*
const cacheObject = {
    '/slot': (24 * 3600) * 1000,
    '/matches': (3 * 3600) * 1000,
    '/casino': (24 * 3600) * 1000,
    '/live': 60 * 1000,
    '/betongames': (24 * 3600) * 1000,
    '/tombalalive': (24 * 3600) * 1000,
    '/init': (24 * 3600) * 1000,
}

const cacheUpdate = (path) => {
    if (cacheObject[path]) {
        fetch(path, this.getHeader())
            .then(res => res.json())
            .then(data => {
                if (data) {
                    localStorage.setItem(path, Date.now())
                    //localForage.setItem(path, data)
                }
            })
    }
}

axios.defaults.baseURL = '//core.me/api';
axios.defaults.headers.common['Authorization'] = "Bearer 123456";

let macIds = _.groupBy(allOdds, 'mac_id');
let demot =  _.mapObject(macIds, (item, key, collection) => {
collection[key] = _.groupBy(item, 'hitit_oran_id')
return collection[key];

})
_.each(_.groupBy(allOdds, 'mac_id'), (oddsOfMatches, match_id) => {
    this.oranTest[match_id] = {}
    _.each(_.groupBy(oddsOfMatches, 'hitit_oran_id'), (oddsOfMatch, market_id) => {
    this.oranTest[match_id][market_id] = _.indexBy(oddsOfMatch, 'oran_id');
    });
});
localStorage.setItem('/marketOdds', JSON.stringify(this.oranTest));
*/