Vue.mixin({
    methods: {
        getLocalData: function (meta_key, cb) {
            localforage.getItem('site_settings_' + localStorage.getItem('site_id'))
                .then(data => cb(data[meta_key]))
        },
        axios_test: function (url, pdata, cb) {
            axios.post(url, pdata)
                .then(function (response) {
                    cb(response)
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        erkenListe: function () {
            this.get('/json/mock.json', (response) => {
                this.maclar = response.Maclar2
            })
        },
        /* Merge Methods */
        mergeData(reponse = {}) {
            this.mergeProcess('/markets', reponse.markets)
            let markets = reponse.markets
            let ligs = reponse.ligs
            let countries = reponse.countries
            let games = reponse.games

        },
        mergeProcess(key = 0, data = {}) {
            let localData = JSON.parse(localStorage.getItem(key))
            localStorage.setItem('/markets', JSON.stringify(localData))
        },
    },
    computed: {},
    mounted() {},
    beforeCreate() {},
    created() {},
    beforeUpdate() {},
    updated() {},
    filters: {},
    components: {}
});