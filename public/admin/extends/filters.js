Vue.filter("json", value => {
    if (!value) return value;
    return JSON.stringify(value, null, 2);
});