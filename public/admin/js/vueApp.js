'use strict';
var Main = httpVueLoader('./vue/App.vue');
var Dashboard = httpVueLoader('./vue/dashboard.vue');
var User = httpVueLoader('./vue/user.vue');
var Users = httpVueLoader('./vue/users.vue');
var Sidebar = httpVueLoader('./vue/sidebar.vue');
var AttributeGroup = httpVueLoader('./vue/attribute_group.vue');
var AddAttributeGroup = httpVueLoader('./vue/add-attribute-group.vue');
var AddTaxonomy = httpVueLoader('./vue/add_taxonomy.vue');
var AttributeGroupItems = httpVueLoader('./vue/attribute_group_items.vue');
var AttributeSet = httpVueLoader('./vue/attribute_set.vue');
var Attributes = httpVueLoader('./vue/attributes.vue');
var AttributeGroupAdd = httpVueLoader('./vue/add-group.vue');
var Post = httpVueLoader('./vue/posts.vue');
var AddPost = httpVueLoader('./vue/add-posts.vue');
var AddAttribute = httpVueLoader('./vue/add-attribute.vue');
var ShowAttribute = httpVueLoader('./vue/edit-attribute.vue');
var Entry = httpVueLoader('./vue/entry.vue');
var Entity = httpVueLoader('./vue/entity.vue');
var Entries = httpVueLoader('./vue/entries.vue');
var Entities = httpVueLoader('./vue/entities.vue');
var Home = httpVueLoader('./vue/home.vue');
var Brand = httpVueLoader('./vue/brand.vue');
var Category = httpVueLoader('./vue/category.vue');
var Roles = httpVueLoader('./vue/roles.vue');
var Categories = httpVueLoader('./vue/categories.vue');
var UserList = httpVueLoader('./vue/user-list.vue');
var Login = httpVueLoader('./vue/login.vue');
var Erken = httpVueLoader('./vue/erken.vue');
var NamedWrapper = httpVueLoader('./vue/named.vue');
var Iframe = httpVueLoader('./vue/iframe.vue');






//var arr = [ 'a', 'b', 'c', 'd', 'e'];
//arr.move(3,1);//["a", "d", "b", "c", "e"]


//var arr = [ 'a', 'b', 'c', 'd', 'e'];
//arr.move(0,2);//["b", "c", "a", "d", "e"]
/* Router and App setup: */
var routes = [
	{
		path: '/',
		name: 'dashboard',
		component: Dashboard,
		children: [
            {
				path: '/',
				name: 'home',
				props: true,
				component: Home
			},
			{
				path: '/post/:post_id',
				name: 'posts',
				props: true,
				component: Post
			},
			{
				path: '/attributes/group',
				name: 'AttributeGroup',
				props: true,
				component: AttributeGroup
			},
			{
				path: '/attributes/add-attribute-group/:entityTypeId/:groupId?',
				name: 'AddAttribute',
				props: true,
				component: AddAttributeGroup
			},
			{
				path: '/attributes/add-attribute/:entityTypeId/:attributeId?',
				name: 'AddAttribute',
				props: true,
				component: AddAttribute
			},
            {
                path: '/attributes/group/add-group',
                name: 'AttributeGroupAdd',
                props: true,
                component: AttributeGroupAdd
            },
			{
				path: '/attributes/group/:group_id',
				name: 'AddAttributeGroup',
				props: true,
				component: AddAttributeGroup
			},
			{
				path: '/add_taxonomy/:entity_type',
				name: 'AddTaxonomy',
				props: true,
				component: AddTaxonomy
			},
			{
				path: '/attributes/set',
				name: 'AttributeSet',
				props: true,
				component: AttributeSet
			},
			{
				path: '/attributes',
				name: 'Attributes',
				props: true,
				component: Attributes
			},
			{
				path: '/attributes/show/:attribute_id',
				name: 'ShowAttribute',
				props: true,
				component: ShowAttribute
			},
			{
				path: '/entry/:entry_id',
				name: 'entry',
				props: true,
				component: Entry
			},
			{
				path: '/entry/:entity_type/add',
				name: 'AddPost',
				props: true,
				component: AddPost
			},
			{
				path: '/entry/:entity_type/:entry_id',
				name: 'EditPost',
				props: true,
				component: Entry
			},
			{
				path: '/entries/:entity_type',
				name: 'entries',
				props: true,
				component: Entries
			},
			{
				path: '/entities',
				name: 'entities',
				props: true,
				component: Entities
			},
			{
				path: '/entities/:entity_type',
				name: 'entities',
				props: true,
				component: Post
			},
			{
				path: '/entity/:entity_type',
				name: 'entity',
				props: true,
				component: Entity
			},
			{
				path: '/categories/:catId?',
				name: 'categories',
				props: true,
				component: Categories
			},
			{
				path: '/users',
				name: 'users',
				props: true,
				component: Users
			},			{
				path: '/roles',
				name: 'Roles',
				props: true,
				component: Roles
			},
			{
				path: '/user/:userId?',
				name: 'user',
				props: true,
				component: User
			},
			{
				path: '/iframe',
				name: 'iframe',
				props: true,
				component: Iframe
			}
		]
	},
	{
		path: '/login',
		name: 'Login',
		component: Login
	}
];

var router = new VueRouter({
	//mode: 'history',
	routes: routes,
	scrollBehavior(to, from) {
		/*if (savedPosition) {
          return savedPosition
        } else {
          return { x: 0, y: 0 }
        }*/
		return {
			x: 0,
			y: 0
		};
	}
});

router.beforeEach((to, from, next) => {
	//console.log(Date.now());

	next();
});

router.afterEach((to, from) => {
	//console.log(Date.now());
});

var app = new Vue({
	router: router,
	el: '#app',
	render: (h) => h(Main),
	template: '<Main/>',
	components: {
		Main,
		'sidebar-marka': Sidebar
	},
	data() {
		return {};
	},
	methods: {
		linkGo(path) {
			this.$router.push(path);
		}
    },
    created() {
        let userData = localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')) : {}
        if(!this.$root.checkLogin(userData)){
			this.$router.push('/login')
		}
    },
    mounted(){
        this.$root.getEntityTypes()
        this.$root.getTermsField()
    }
});
