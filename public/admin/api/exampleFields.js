var exampleFields = {
    "text": {
        "key": "field_5d381cc216415",
        "label": "metin 01",
        "name": "metin_01",
        "type": "text",
        "instructions": "",
        "required": 0,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "default_value": "",
        "placeholder": "",
        "prepend": "",
        "append": "",
        "maxlength": ""
    },
    "select": {
        "key": "field_5d381f9b16416",
        "label": "selectbox2",
        "name": "selectbox2",
        "type": "select",
        "instructions": "Instructions\r\nInstructions for authors. Shown when submitting data",
        "required": 1,
        "conditional_logic": [
            [
                {
                    "field": "field_5d381cc216415",
                    "operator": "==",
                    "value": "bir"
                }
            ],
            [
                {
                    "field": "field_5d381cc216415",
                    "operator": "!=",
                    "value": "iki"
                }
            ]
        ],
        "wrapper": {
            "width": "200",
            "class": "class1",
            "id": "id25"
        },
        "choices": {
            "red:Red": "red:Red",
            "yellow:Yellow": "yellow:Yellow"
        },
        "default_value": [
            "red:Red"
        ],
        "allow_null": 0,
        "multiple": 1,
        "ui": 1,
        "ajax": 1,
        "return_format": "array",
        "placeholder": ""
    },
    "taxonomy": {
        "key": "field_5d53223ab1d38",
        "label": "tax01",
        "name": "tax01",
        "type": "taxonomy",
        "instructions": "",
        "required": 0,
        "conditional_logic": [
            [
                {
                    "field": "field_5d381f9b16416",
                    "operator": "==",
                    "value": "yellow"
                }
            ]
        ],
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "taxonomy": "category",
        "field_type": "multi_select",
        "allow_null": 1,
        "add_term": 1,
        "save_terms": 1,
        "load_terms": 1,
        "return_format": "object",
        "multiple": 0
    },
    "image": {
        "key": "field_5d532271c079e",
        "label": "image01",
        "name": "image01",
        "type": "image",
        "instructions": "",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "100",
            "class": "class01",
            "id": "idname"
        },
        "return_format": "array",
        "preview_size": "medium",
        "library": "uploadedTo",
        "min_width": 100,
        "min_height": 100,
        "min_size": 10,
        "max_width": 150,
        "max_height": 200,
        "max_size": 15,
        "mime_types": "jpg"
    },
    "textarea": {
        "key": "field_5d537e8b71c88",
        "label": "labeltextare",
        "name": "labeltextare",
        "type": "textarea",
        "instructions": "açıklama",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "100",
            "class": "as",
            "id": "asd"
        },
        "default_value": "default vlue",
        "placeholder": "placetext",
        "maxlength": 500,
        "rows": 7,
        "new_lines": "wpautop"
    },
    "number": {
        "key": "field_5d537eef71c89",
        "label": "numbertest01",
        "name": "numbertest01",
        "type": "number",
        "instructions": "intro",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "10",
            "class": "10",
            "id": "10"
        },
        "default_value": 10,
        "placeholder": "place",
        "prepend": "pre",
        "append": "ap",
        "min": 50,
        "max": 100,
        "step": 9
    },
    "range": {
        "key": "field_5d537f1971c8a",
        "label": "range01",
        "name": "range01",
        "type": "range",
        "instructions": "int",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "10",
            "class": "10",
            "id": "10"
        },
        "default_value": 10,
        "min": 10,
        "max": 100,
        "step": 2,
        "prepend": "pre",
        "append": "ap"
    },
    "email": {
        "key": "field_5d537f6b71c8b",
        "label": "email01",
        "name": "email01",
        "type": "email",
        "instructions": "int",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "10",
            "class": "10",
            "id": "10"
        },
        "default_value": "em@email.com",
        "placeholder": "place",
        "prepend": "pre",
        "append": "ap"
    },
    "url": {
        "key": "field_5d537f8c71c8c",
        "label": "url01",
        "name": "url01",
        "type": "url",
        "instructions": "int",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "10",
            "class": "10",
            "id": "10"
        },
        "default_value": "def",
        "placeholder": "plac"
    },
    "password": {
        "key": "field_5d537fb971c8d",
        "label": "pass01",
        "name": "pass01",
        "type": "password",
        "instructions": "int",
        "required": 0,
        "conditional_logic": 0,
        "wrapper": {
            "width": "10",
            "class": "10",
            "id": "10"
        },
        "placeholder": "pl",
        "prepend": "pre",
        "append": "ap"
    },
    "file": {
        "key": "field_5d537fce71c8e",
        "label": "file01",
        "name": "file01",
        "type": "file",
        "instructions": "int",
        "required": 0,
        "conditional_logic": 0,
        "wrapper": {
            "width": "20",
            "class": "20",
            "id": "20"
        },
        "return_format": "array",
        "library": "all",
        "min_size": 10,
        "max_size": 100,
        "mime_types": "pdf,png"
    },
    "wysiwyg": {
        "key": "field_5d537ff671c8f",
        "label": "editor01",
        "name": "editor01",
        "type": "wysiwyg",
        "instructions": "int",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "10",
            "class": "10",
            "id": "10"
        },
        "default_value": "defv",
        "tabs": "all",
        "toolbar": "full",
        "media_upload": 1,
        "delay": 1
    },
    "oembed": {
        "key": "field_5d53801c71c90",
        "label": "oembed01",
        "name": "oembed01",
        "type": "oembed",
        "instructions": "int",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "10",
            "class": "10",
            "id": "10"
        },
        "width": 600,
        "height": 400
    },
    "link": {
        "key": "field_5d53803d71c91",
        "label": "link01",
        "name": "link01",
        "type": "link",
        "instructions": "int",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "10",
            "class": "10",
            "id": "10"
        },
        "return_format": "array"
    },
    "checkbox": {
        "key": "field_5d53805271c92",
        "label": "check01",
        "name": "check01",
        "type": "checkbox",
        "instructions": "",
        "required": 0,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "choices": {
            "red:kirmizi": "red:kirmizi"
        },
        "allow_custom": 0,
        "default_value": [
            "red"
        ],
        "layout": "horizontal",
        "toggle": 1,
        "return_format": "array",
        "save_custom": 0
    },
    "radio": {
        "key": "field_5d53809d71c93",
        "label": "radio01",
        "name": "radio01",
        "type": "radio",
        "instructions": "",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "choices": {
            "red:Red": "red:Red"
        },
        "allow_null": 1,
        "other_choice": 1,
        "save_other_choice": 1,
        "default_value": "red",
        "layout": "vertical",
        "return_format": "array"
    },
    "button_group": {
        "key": "field_5d5380c371c94",
        "label": "button",
        "name": "button",
        "type": "button_group",
        "instructions": "",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "choices": {
            "red:Red": "red:Red"
        },
        "allow_null": 1,
        "default_value": "red",
        "layout": "vertical",
        "return_format": "array"
    },
    "true_false": {
        "key": "field_5d5380e071c95",
        "label": "truefalse01",
        "name": "truefalse01",
        "type": "true_false",
        "instructions": "",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "message": "message",
        "default_value": 1,
        "ui": 1,
        "ui_on_text": "yeson",
        "ui_off_text": "nooff"
    },
    "post_object": {
        "key": "field_5d538121b2911",
        "label": "postobject01",
        "name": "postobject01",
        "type": "post_object",
        "instructions": "",
        "required": 0,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "post_type": [
            "post",
            "attachment",
            "page"
        ],
        "taxonomy": [
            "category:anne-ve-bebek",
            "category:elektronik"
        ],
        "allow_null": 1,
        "multiple": 1,
        "return_format": "object",
        "ui": 1
    },
    "page_link": {
        "key": "field_5d538155b2912",
        "label": "pagelink01",
        "name": "pagelink01",
        "type": "page_link",
        "instructions": "",
        "required": 0,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "post_type": [
            "post",
            "attachment"
        ],
        "taxonomy": [
            "category:elektronik"
        ],
        "allow_null": 1,
        "allow_archives": 1,
        "multiple": 1
    },
    "relationship": {
        "key": "field_5d538176b2913",
        "label": "relationship01",
        "name": "relationship01",
        "type": "relationship",
        "instructions": "",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "post_type": [
            "post"
        ],
        "taxonomy": [
            "category:bilet-tatil-ve-eglence",
            "category:beyaz-esya"
        ],
        "filters": [
            "search",
            "post_type",
            "taxonomy"
        ],
        "elements": [
            "featured_image"
        ],
        "min": 10,
        "max": 20,
        "return_format": "id"
    },
    "user": {
        "key": "field_5d538199b2914",
        "label": "user01",
        "name": "user01",
        "type": "user",
        "instructions": "",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "role": [
            "contributor",
            "administrator",
            "subscriber"
        ],
        "allow_null": 1,
        "multiple": 1,
        "return_format": "array"
    },
    "google_map": {
        "key": "field_5d53821db2915",
        "label": "map01",
        "name": "map01",
        "type": "google_map",
        "instructions": "",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "center_lat": "-4554.565",
        "center_lng": "144.444",
        "zoom": 14,
        "height": 400
    },
    "date_picker": {
        "key": "field_5d538246b2916",
        "label": "datepicker01",
        "name": "datepicker01",
        "type": "date_picker",
        "instructions": "dsd",
        "required": 0,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "display_format": "m/d/Y",
        "return_format": "F j, Y",
        "first_day": 3
    },
    "date_time_picker": {
        "key": "field_5d538272b2917",
        "label": "datetimepicker01",
        "name": "datetimepicker01",
        "type": "date_time_picker",
        "instructions": "df",
        "required": 1,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "display_format": "m/d/Y g:i a",
        "return_format": "m/d/Y g:i a",
        "first_day": 5
    },
    "time_picker": {
        "key": "field_5d53828ab2918",
        "label": "timepicker01",
        "name": "timepicker01",
        "type": "time_picker",
        "instructions": "",
        "required": 0,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "display_format": "H:i:s",
        "return_format": "H:i:s"
    },
    "color_picker": {
        "key": "field_5d53831eb2919",
        "label": "colorpicker01",
        "name": "colorpicker01",
        "type": "color_picker",
        "instructions": "",
        "required": 0,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "default_value": "#FFFFFF"
    },
    "message": {
        "key": "field_5d538337b291a",
        "label": "message01",
        "name": "",
        "type": "message",
        "instructions": "",
        "required": 0,
        "conditional_logic": 0,
        "wrapper": {
            "width": "",
            "class": "",
            "id": ""
        },
        "message": "message",
        "new_lines": "br",
        "esc_html": 1
    }
}


var exampleFieldsTypes = {
    "text": {},
    "textarea": {},
    "number": {},
    "range": {},
    "email": {},
    "url": {},
    "password": {},
    "price": {},
    "float": {},
    "double": {},
    "hidden": {},
    "image": {},
    "file": {},
    "wysiwyg": {},
    "oembed": {},
    "select": {},
    "checkbox": {},
    "radio": {},
    "button_group": {},
    "true_false": {},
    "content": {},
    "media": {},
    "relationship": {},
    "taxonomy": {},
    "user": {},
    "google_map": {},
    "date_picker": {},
    "date_time_picker": {},
    "time_picker": {},
    "color_picker": {},
    "message": {},
    "accordion": {},
    "tab": {},
    "group": {}
}
