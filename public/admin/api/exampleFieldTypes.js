var fieldTypes = {
	"Basic": {
		"text": {},
		"textarea": {},
		"number": {},
		"range": {},
		"email": {},
		"url": {},
		"password": {},
		"price": {},
		"float": {},
		"double": {},
		"hidden": {}
	},
	"Content": {
		"image": {},
		"file": {},
		"wysiwyg": {},
		"oembed": {}
	},
	"Choice": {
		"select": {},
		"checkbox": {},
		"radio": {},
		"button_group": {},
		"true_false": {}
	},
	"Relational": {
		"content": {},
		"media": {},
		"relationship": {},
		"taxonomy": {},
		"user": {}
	},
	"Custom": {
		"google_map": {},
		"date_picker": {},
		"date_time_picker": {},
		"time_picker": {},
		"color_picker": {}
	},
	"Layout": {
		"message": {},
		"accordion": {},
		"tab": {},
		"group": {}
	}
}

