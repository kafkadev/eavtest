<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'SLIR\\' => array($baseDir . '/core'),
    'Risko\\' => array($baseDir . '/icc'),
);
