<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */


    public function json_contains($json, $val) {
        $array = json_decode($json, true);
        // trim double quotes from around the value to match MySQL behaviour
        $val = trim($val, '"');
        // this will work for a single dimension JSON value, if more dimensions
        // something more sophisticated will be required
        // that is left as an exercise for the reader
        return in_array($val, $array);
      }

    public function register()
    {

        //JSON_CONTAINS(attr, '[\"Garmin\"]', 'marka')
        if (DB::Connection() instanceof \Illuminate\Database\SQLiteConnection) {
            DB::connection()->getPdo()->sqliteCreateFunction('JSON_CONTAINS', function ($json, $val, $path = null) {
                $val = json_decode($val, 1);
                $array = json_decode($json, true);
                //print_r($val);
               // die;
                $data = [];
                //eval('$data = isset($array["places"]["entities"]) ? $array["places"]["entities"] : [];');

                $path = explode('.', $path);
                if ($val && isset($array[$path[0]])) {
                    return in_array($val[0], $array[$path[0]]);
                //return $array[$path] == $val;
                }
                else {

                    //$val = trim($val, '"');
                      return 0;
                }
            });
        }
    }
}
