<?php
//pass
namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use SimpleXMLElement;
class crawler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'robot:crawler01  {url : The URL to check}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $url = $this->getUrl();
      $this->sitemapIndexParse($url);
    }
    public function startCommand($value='')
    {
      $this->info('Url denetleniyor...');
      $url = $this->getUrl();
      if ($url) {

        $dom = $this->getDom($url, true);



        $body = $dom->find('li a.IC > span > h3');


        foreach ($body as $key => $content)
        {

          $this->line($content->text);
        }
      } else {
        print_r('url yok');
      }
    }
    private function getUrl()
    {
      $url = $this->argument('url');

      if (! filter_var($url, FILTER_VALIDATE_URL)) {
        throw new \Exception("Invalid URL '$url'");
      }

      return $url;
    }

    public function getDom($url=0, $encode = true)
    {
      $dom = 0;
      if ($url) {
        mb_internal_encoding("UTF-8");

        $html = file_get_contents($url, null, stream_context_create(array(
          'http' => array('ignore_errors'=> true,'timeout' => 10, 'header' => "User-Agent:MyAgent/1.0"),
          'ssl' => array('verify_peer' => false, 'verify_peer_name' => false)
        )));

        $html = str_replace('<!DOCTYPE html>', "", $html);
        if ($encode) {
         $html = mb_convert_encoding($html, 'UTF-8', 'windows-1254');
   //   $html = mb_ereg_replace('\s+', ' ', $html);
   //   $html = mb_convert_encoding($html, 'HTML-ENTITIES', "UTF-8");
       }

       $dom = new Dom;
       $dom->loadStr($html, [
        'removeScripts' => true, // Set a global option to enable strict html parsing.
        'removeStyles' => true, // Set a global option to enable strict html parsing.
        'htmlSpecialCharsDecode' => false, // Set a global option to enable strict html parsing.
        'whitespaceTextNode' => false,
        'enforceEncoding'=> 'UTF-8'
      ]);
     }
     return $dom;
   }
   public function sitemapIndexParse($sitemapIndexUrl){
    $this->info('sitemapIndexParse denetleniyor...');
    if ($sitemapIndexUrl) {
     $sitemapContent = $this->getFileContent($sitemapIndexUrl);
     $xml = new SimpleXMLElement($sitemapContent);
     foreach ($xml->sitemap as $key => $url) {
      $this->sitemapUrlParse($url->loc);

    }

  }

}

public function sitemapUrlParse($sitemap_url){
  $this->info('sitemapUrlParse denetleniyor...');

  if ($sitemap_url) {
   $sitemapContent = $this->getFileContent($sitemap_url);
   $xml = new SimpleXMLElement($sitemapContent);
   $arr = [];
   foreach ($xml->url as $url) {

    $category = explode('/', $url->loc)[3];
    print_r($category.PHP_EOL);
    $arr[] = [$category,$url->loc];
  }
  $this->generateCsv($arr);

}

}

public function getFileContent($url='')
{
  return file_get_contents($url, null, stream_context_create(array(
    'http' => array('ignore_errors'=> true,'timeout' => 10, 'header' => "User-Agent:MyAgent/1.0"),
    'ssl' => array('verify_peer' => false, 'verify_peer_name' => false)
  )));
}

public function generateCsv($data = [], $key = null, $delimiter = ',', $enclosure = '"') {
  $this->info('generateCsv denetleniyor...');

  $handle = fopen('sitemapEpey.csv', 'a');
  foreach ($data as $line) {
   fputcsv($handle, $line, $delimiter, $enclosure);
 }
 fclose($handle);
}



}
