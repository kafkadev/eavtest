<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use SimpleXMLElement;
use Illuminate\Support\Facades\DB;
use App\Models\Content;
use App\Models\EntryType;
use App\Models\Entity;
use App\Models\TermEntity;
use App\Models\Term;
use App\Models\TermVariant;
use Illuminate\Support\Str;
use App\Models\EavAttribute;

/*
say: SELECT cat, count(cat) FROM epeyLinks GROUP BY cat ORDER BY count(cat) DESC



 */



class CreateEntityContentImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:create-group-meta-import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Entity içeriğine json meta value oluşturur.';
    protected $entityTypeId = 31;
    protected $termTypeId = 29;
    protected $exampleEntity = '{
      "title": "",
      "description": "",
      "entity_status": "draft",
      "open_comment": false,
      "open_shareble": true,
      "publish_date": "0000-00-00",
      "taxonomy_category": 0,
      "taxonomy_tags": [],
      "specs": {}
    }';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      //print_r(Str::camel(Str::slug('asd ASd')));
    // $this->generateEntityGroupsCimri();
      $this->generateEntityGroupsEpey();
      print_r('bitti');
    }



    public function generateEntityGroupsEpey()
    {

     $groups = DB::connection('epeyDetails')->table('attrs')
     ->where('type','meta')
     ->get()->toArray();
     $cats = collect($groups)->groupBy('cat')->toArray();


     foreach ($cats as $cat => $catArr) {
        print_r($cat.'...'.PHP_EOL);

     $catId = $this->insertTermVariant(29, $cat);
      $groupNames = collect($catArr)->groupBy('groupName')->toArray();

     foreach ($groupNames as $groupName => $attrs) {
        $groupId = $this->addGroup($cat, $catId, $groupName, 31);
        $attributeNames = collect($attrs)->groupBy('metaName')->toArray();
        $attrArr = [];
        print_r($groupName.'...'.PHP_EOL);
        foreach ($attributeNames as $attrName => $attr) {
          $values = collect($attr)->pluck('metaValue')->toArray();
          $attrArr[] = $this->addAttribute($attrName, $values, $groupId, 31);

        }

        EavAttribute::insert($attrArr);
      }

    }

  }
  public function generateEntityGroupsCimri()
  {
/*$results = DB::select( DB::raw("SELECT * FROM some_table WHERE some_col = :somevariable"), array(
   'somevariable' => $someVariable,
 ));*/
 $groups = DB::connection('cimriDetails')->table('specGroups')
 ->get()->toArray();
 $cats = collect($groups)->groupBy('newCatId')->toArray();


 foreach ($cats as $catId => $catArr) {

  $groupNames = collect($catArr)->groupBy('groupName')->toArray();

  foreach ($groupNames as $groupName => $attrs) {
    $groupId = $this->addGroup($catId, $catId, $groupName, 30);
    $attributeNames = collect($attrs)->groupBy('metaName')->toArray();
    $attrArr = [];
    print_r($groupName.'...'.PHP_EOL);
    foreach ($attributeNames as $attrName => $attr) {
      $values = collect($attr)->pluck('metaValue')->toArray();
      $attrArr[] = $this->addAttribute($attrName, $values, $groupId, 30);

    }

    EavAttribute::insert($attrArr);
  }

}

}

public function addGroup($category, $catId, $groupName,$entityTypeId)
{





  $groupId = DB::table('eav_attribute_group')->insertGetId(
    [
      "is_primary" => 0,
      "places" => json_encode(["terms" => [$catId]]),
      "sort_order" => 0,
      "set_group_id" => 1,
      "entity_type_id" => $entityTypeId,
      "description" => $category.'/'.$groupName,
      "tab_group_id" => 1,
      "selected_terms" => json_encode([strval($catId)]),
      "attribute_group_name" => Str::title($groupName),
      "attribute_group_code" => Str::slug($groupName,'_'),
      "status" => 1
    ]);

  return $groupId;
}

public function addAttribute($attrName, $values, $groupId, $entityTypeId)
{

  $jsonData = '{
    "data": {
      "wrapper": {
        "id": "id1",
        "class": "class1",
        "width": "200"
        },
        "required": 1,
        "name": "field01",
        "default_value": [],
        "return_format": "array",
        "multiple": 1,
        "label": "field01",
        "placeholder": "",
        "allow_null": 0,
        "ui": 1,
        "instructions": "",
        "key": "",
        "choices_arr": {},
        "choices": "",
        "type": "select",
        "conditional_logic": [],
        "ajax": 1
        },
        "is_unique": true,
        "attribute_code": "",
        "note": "",
        "help_description": "",
        "field_type": "select",
        "backend_type": "varchar",
        "frontend_label": "",
        "entity_type_id": 0,
        "is_required": false,
        "is_user_defined": true,
        "frontend_input": "select",
        "is_relational": false
      }';
  /*print_r($values);
  print_r(implode("\n", $values));
  die();*/
  $jsonData = json_decode($jsonData, 1);
  $jsonData['data']['choices'] = (string)implode("\n", $values);
  $jsonData['attribute_group_id']= $groupId;
  $jsonData['attribute_code']= Str::slug($attrName,'_');
  $jsonData['frontend_label']= Str::title($attrName);
  $jsonData['entity_type_id']= $entityTypeId;
  $jsonData['data'] = json_encode($jsonData['data']);
  return $jsonData;
}

public function insertTermVariant($term_type_id, $term_name, $parent_id = 0, $level = 0, $path = '')
{
        //$parent_id = 0;
        //$level = 0;
        //$count = 0;
  $term = $this->insertTermValue($term_name);
  $termId = $term->term_id;

  $termTax = TermVariant::where('term_id', $termId)->where('term_type_id', $term_type_id)->first();
  if (!$termTax) {
    $termTax = TermVariant::create(['term_type_id' => $term_type_id, 'term_id' => $termId, 'parent_id' => $parent_id, 'level' => $level, 'path' => '']);
            //$termTax->save();
  }

  //$termTax->path = implode('/', $termTax->getPathText());
  //$termTax->save();

  return $termTax->variant_id;
}

public function insertTermValue($term_name)
{
  $term = Term::firstOrNew(['name' => $term_name, 'slug' => Str::slug($term_name)]);

  return $term;
}


public function insertTerm3($termName, $type_id)
{
  $termTaxId = 0;
  $term      = DB::table('terms')->select('term_id')->where('name', $termName)->orderBy('term_id','desc')->first();
  if (!$term) {
    $termId = DB::table('terms')->insertGetId(['name' => $termName, 'slug' => Str::slug($termName,'_')]);
  } else {
    $termId = $term->term_id;

  }
  $termTax = DB::table('term_variant')->where('term_id', $termId)->where('term_type_id', $type_id)->first();
  if ($termTax) {
    $termTaxId = $termTax->variant_id;
  } else {
    $termTaxId = DB::table('term_variant')->insertGetId(['term_id' => $termId, 'term_type_id' => $type_id]);
  }
  return $termTaxId;
}

public function insertTerm2($termName, $type_id)
{
  $termTaxId = 0;





  $termTax = DB::table('term_variant')
  ->leftJoin('terms', 'terms.term_id', '=', 'term_variant.term_id')->where('terms.slug', $termName)->where('term_variant.term_type_id', $type_id)->first();
  if (!$termTax) {
    print_r($termName);
    die();
  }
  else {

    $termTaxId = $termTax->variant_id;
  }
  return $termTaxId;
}






}
