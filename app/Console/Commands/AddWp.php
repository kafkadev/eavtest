<?php
//pass
namespace App\Console\Commands;

use App\Models\Content;
use App\Models\ContentMeta;
use DB;
use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use SimpleXMLElement;

/*
say: SELECT cat, count(cat) FROM epeyLinks GROUP BY cat ORDER BY count(cat) DESC

 */

class AddWp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:addwp {count : The wp posts count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sampleContent = [
        'entity_status' => 'draft',
        'entity_type_id' => 1
    ];

       /* $wpdb            = DB::connection('sqlitewp');
        $items           = $wpdb->table('wp_posts')->limit(10)->get()->map(function ($item) {
            return [
                'title'   => $item->post_title,
                'content' => $item->post_content,
            ];
        });*/

        $this->getPaginateRun(1);

    }

    public function exportNode($node = '')
    {
        $res = [];
        if (count($node->find('span')) > 1) {
            foreach ($node->find('span') as $content) {
                $res[] = strip_tags($content->innerHtml);
            }
        } else {
            $res = strip_tags($node->innerHtml);
        }
        return $res;
    }

    private function getUrl()
    {
        $url = $this->argument('url');

        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \Exception("Invalid URL '$url'");
        }

        return $url;
    }

    private function oneLinkRun($url = 0)
    {
        if ($url) {

            $dom      = $this->getDom($url);
            $urlParse = parse_url($url);
            $cat      = explode('/', $urlParse['path'])[1];
            $insetArr = [
                'title'    => '',
                'aile'     => '',
                'marka'    => '',
                'resimler' => [],
                'content'  => [],
            ];

            $insetArr['title'] = $dom->find('.baslik h1 a')[0]->getAttribute('title');
            $insetArr['marka'] = stristr($insetArr['title'], ' ', true);
            $insetArr['aile']  = count($dom->find('.baslik h1 a span.aile')) > 0 ? $dom->find('.baslik h1 a span.aile')[0]->text : 0;
            $insetArr['kod']   = count($dom->find('.baslik h1 a span.kod')) > 0 ? $dom->find('.baslik h1 a span.kod')[0]->text : 0;
            $resimler          = $dom->find('.buyuk .galerim img');

            if (count($resimler)) {
                foreach ($resimler as $key => $imgItem) {
                    $insetArr['resimler'][] = $imgItem->getAttribute('src');
                }
            }

            $body = $dom->find('#bilgiler #grup');

            $total  = [];
            $classa = [];

            foreach ($body as $groupKey => $groupContent) {
                $groupTitle         = $groupContent->find('h3 > span')[0]->text;
                $total[$groupTitle] = [];
                foreach ($groupContent->find('ul.grup li') as $key => $content) {
                    $classa               = [];
                    $classa['key']        = $content->find('strong')[0]->text;
                    $valueisArray         = $content->find('span')[0];
                    $classa['value']      = $this->exportNode($valueisArray);
                    $total[$groupTitle][] = $classa;
                }
            }
            $insetArr['content'] = $total;

            \DB::table('entity2')->insert(['entity_type_id' => 4, 'title' => $insetArr['title'], 'cat' => $cat, 'attributes' => json_encode($insetArr)]);

        }
    }

    public function generateBrand()
    {
        $termId     = 0;
        $collection = DB::table('entity2')->get();
        foreach ($collection as $key => $value) {
            $content   = json_decode($value->attributes);
            $termName  = $content->marka;
            $termTaxId = $this->insertTerm($termName, 15);
            DB::table('term_entity')->updateOrInsert(['entity_id' => $value->entity_id, 'variant_id' => $termTaxId], ['entity_id' => $value->entity_id, 'variant_id' => $termTaxId]);

        }
    }

    public function getPaginateRun($page = 1)
    {
        $post_type   = 'articles';
        $db = DB::connection('sqlitewp');
        $range = range(1, $db->table('wp_posts')->where('post_type', $post_type)->count() / 10);

        foreach ($range as $key => $sayfa) {
            print_r('sayfa:' . $sayfa . "\n");
            # code...
            $collect = $db->table('wp_posts')->where('post_type', $post_type)->limit(10)->offset(($sayfa - 1) * 10)->get()->toArray();
            foreach ($collect as $arid => $item) {
                $sampleContent = [
                    'entity_status' => 'draft',
                    'entity_type_id' => 1
                ];
                $sampleContentMeta = [
                    'title'   => $item->post_title,
                    'content' => strip_tags($item->post_content, "<b><span><p><br>"),
                ];
                $entityContentId = Content::insertGetId($sampleContent);
                $entityContent   = ContentMeta::create(['content_id' => $entityContentId, 'attributes' => $sampleContentMeta]);

                print_r('sayfa:' . $sayfa . ':' . $arid . "\n");
            }
        }
    }

    public function insertTerm($termName, $type_id)
    {
        $termTaxId = 0;
        $term      = DB::table('terms')->select('term_id')->where('name', $termName)->first();
        if (!$term) {
            $termId = DB::table('terms')->insertGetId(['name' => $termName, 'slug' => $termName]);
        } else {
            $termId = $term->term_id;

        }
        $termTax = DB::table('term_variant')->where('term_id', $termId)->where('term_type_id', $type_id)->first();
        if ($termTax) {
            $termTaxId = $termTax->variant_id;
        } else {
            $termTaxId = DB::table('term_variant')->insertGetId(['term_id' => $termId, 'term_type_id' => $type_id]);
        }
        return $termTaxId;
    }

    private function catLinkRun($url = 0)
    {
        if ($url) {

            $dom      = $this->getDom($url);
            $body     = $dom->find('li .detay a');
            $lastPage = $dom->find('a.son')[0]->getAttribute('href');
            $nextPage = $dom->find('a.ileri')[0]->getAttribute('href');

            $pageObject = ['last_page' => $lastPage, 'next_page' => $nextPage, 'links' => []];
            $total      = [];
            $classa     = [];

            foreach ($body as $content) {
                $classa = [];
//print_r($content->innerHtml);
                $classa['title'] = $content->text;
                $classa['value'] = $content->getAttribute('href');
                $total[]         = $classa;
            }

            $pageObject['links'] = $total;

            $urlParse = parse_url($url);
            $cat      = explode('/', $urlParse['path'])[1];
            foreach ($pageObject['links'] as $key => $value) {
                \DB::table('epeyLinks')->insert(['cat' => $cat, 'url' => $value['value']]);
            }

            if ($nextPage || $lastPage) {
                print_r('sonraki sayfa geçildi :' . $nextPage . "\n");
                $this->catLinkRun($nextPage);
                sleep(3);
            }
            print_r('BITTI....');
            die();
//print_r($total);

        }
    }

    public function getDom($url = 0)
    {
        $dom = 0;
        if ($url) {
            $html = file_get_contents($url, false, stream_context_create(array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false))));
//$html = strip_tags($html, '<h1><body><div><p><html>');
            $dom = new Dom;
            $dom->load($html, [
                'removeScripts' => true, // Set a global option to enable strict html parsing.
                'removeStyles'  => true, // Set a global option to enable strict html parsing.
            ]);
        }
        return $dom;
    }

    public function sitemapParse($sitemap_url)
    {
        /* $urlParse = parse_url($sitemap_url);
        $urlParse = explode('/', $urlParse['path'])[1]*/
        if ($sitemap_url) {
            $sitemapContent = file_get_contents($sitemap_url);
            $xml            = new SimpleXMLElement($sitemapContent);

            foreach ($xml->url as $url) {
                $urlParse = parse_url($url->loc);
                $urlParse = explode('/', $urlParse['path'])[1];
                //$locLink = trim($url->loc);
                // $this->sitemapLinks[$locLink] = $category;
                print_r($urlParse . "\n");
            }

        }

    }

    public function nextPage($url)
    {
        /* $urlParse = parse_url($sitemap_url);
        $urlParse = explode('/', $urlParse['path'])[1]*/
        if ($sitemap_url) {
            $sitemapContent = file_get_contents($sitemap_url);
            $xml            = new SimpleXMLElement($sitemapContent);

            foreach ($xml->url as $url) {
                $urlParse = parse_url($url->loc);
                $urlParse = explode('/', $urlParse['path'])[1];
                //$locLink = trim($url->loc);
                // $this->sitemapLinks[$locLink] = $category;
                print_r($urlParse . "\n");
            }

        }

    }

}
