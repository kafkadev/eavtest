<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use SimpleXMLElement;
use Illuminate\Support\Facades\DB;
use App\Models\Content;
use App\Models\EntryType;
use App\Models\Entity;
use App\Models\TermEntity;
use App\Models\Term;
use App\Models\TermVariant;
use Illuminate\Support\Str;
use App\Models\EavAttribute;
use App\Models\EavGroup;

/*
say: SELECT cat, count(cat) FROM epeyLinks GROUP BY cat ORDER BY count(cat) DESC



 */



class CreateBulkTerms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:create-bulk-terms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Entity içeriğine json meta value oluşturur.';
    protected $entityTypeId = 4;
    protected $termTypeId = 28;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      //$this->generateEntityGroupsEpey();
      //$this->insertBulkTerms($this->termTypeId, 0);
      //$this->insertBulkBrands(15, 0);
     // $this->setSpecCats($this->termTypeId, 0);
    $this->insertBulkTermsEpey();
    //$this->fixerAttrsCat();
      print_r('bitti');
    }


    public function generateEntityMetaValue()
    {

      $entities = DB::connection('liberyen')->table('epeyContent')->select('*')->get();
      $entities = collect($entities)->toArray();
      foreach ($entities as $key => $entity) {
        $earr = [];

        $entity->attributes = json_decode($entity->attributes,1);
        foreach ($entity->attributes['content'] as $key => $groupItems) {

          $groupName = $key;

          foreach ($groupItems as $gkey => $gvalue) {

            $optionName = $gvalue['key'];

            if (is_array($gvalue['value'])) {

              foreach ($gvalue['value'] as $keyo => $value) {
               $valueName = $value;
               $options = $this->insertOption($groupName, $optionName, $valueName, $entity->entity_type_id);


             }
           } else {

            $valueName = $gvalue['value'];
            $options = $this->insertOption($groupName, $optionName, $valueName, $entity->entity_type_id);
          }


        }
      }
    }

  }

  public function generateEntityGroupsEpey()
  {
/*$results = DB::select( DB::raw("SELECT * FROM some_table WHERE some_col = :somevariable"), array(
   'somevariable' => $someVariable,
 ));*/
 $groups = DB::connection('liberyen')->table('groupMapsEpey')
      //->leftJoin('meta_labels', 'meta_labels.key', '=', 'groupMapsEpey.group_name')
 ->where('type','meta')
 ->groupBy('cat','group_name', 'attribute','value')
                //->having('type', '=', 'group')
 ->get()->toArray();
 $cats = collect($groups)->groupBy('cat')->toArray();


 foreach ($cats as $cat => $catArr) {
  $catId = $this->insertTerm($cat, $this->termTypeId);
  $groupNames = collect($catArr)->groupBy('group_name')->toArray();

  foreach ($groupNames as $groupName => $attrs) {
    $groupId = $this->addGroup($cat, $catId, $groupName);
    $attributeNames = collect($attrs)->groupBy('attribute')->toArray();
    $attrArr = [];
    foreach ($attributeNames as $attrName => $attr) {
      $values = collect($attr)->pluck('value')->toArray();
      $attrArr[] = $this->addAttribute($attrName, $values, $groupId, $this->entityTypeId);
      //print_r($values);

    }

    EavAttribute::insert($attrArr);
  }

}

}



public function addAttribute($attrName, $values, $groupId, $entityTypeId)
{

  $jsonData = '{
    "data": {
      "wrapper": {
        "id": "id1",
        "class": "class1",
        "width": "200"
        },
        "required": 1,
        "name": "field01",
        "default_value": [],
        "return_format": "array",
        "multiple": 1,
        "label": "field01",
        "placeholder": "",
        "allow_null": 0,
        "ui": 1,
        "instructions": "",
        "key": "",
        "choices_arr": {},
        "choices": "",
        "type": "select",
        "conditional_logic": [],
        "ajax": 1
        },
        "is_unique": true,
        "attribute_code": "",
        "note": "",
        "help_description": "",
        "field_type": "select",
        "backend_type": "varchar",
        "frontend_label": "",
        "entity_type_id": 0,
        "is_required": false,
        "is_user_defined": true,
        "frontend_input": "select",
        "is_relational": false
      }';
  /*print_r($values);
  print_r(implode("\n", $values));
  die();*/
  $jsonData = json_decode($jsonData, 1);
  $jsonData['data']['choices'] = (string)implode("\n", $values);
  $jsonData['attribute_group_id']= $groupId;
  $jsonData['attribute_code']= $attrName;
  $jsonData['frontend_label']= $attrName;
  $jsonData['entity_type_id']= $entityTypeId;
  $jsonData['data'] = json_encode($jsonData['data']);
  return $jsonData;
}





public function generateBrand()
{
  $termId     = 0;
  $collection = DB::table('entity2')->get();
  foreach ($collection as $key => $value) {
    $content   = json_decode($value->attributes);
    $termName  = $content->marka;
    $termTaxId = $this->insertTerm($termName, 15);
  }
}

public function insertTerm($termName, $type_id)
{
  $termTaxId = 0;
  $term      = DB::table('terms')->select('term_id')->where('name', $termName)->first();
  if (!$term) {
    $termId = DB::table('terms')->insertGetId(['name' => $termName, 'slug' => $termName]);
  } else {
    $termId = $term->term_id;

  }
  $termTax = DB::table('term_variant')->where('term_id', $termId)->where('term_type_id', $type_id)->first();
  if ($termTax) {
    $termTaxId = $termTax->variant_id;
  } else {
    $termTaxId = DB::table('term_variant')->insertGetId(['term_id' => $termId, 'term_type_id' => $type_id]);
  }
  return $termTaxId;
}
public function addGroup($category = 0, $catId = 0, $groupName = '0')
{





  $groupId = DB::table('eav_attribute_group')->insertGetId(
    [
      "is_primary" => 0,
      "places" => json_encode(["terms" => [$catId]]),
      "sort_order" => 0,
      "set_group_id" => 1,
      "entity_type_id" => $this->entityTypeId,
      "description" => $category.'/'.$groupName,
      "tab_group_id" => 1,
      "selected_terms" => json_encode([strval($catId)]),
      "attribute_group_name" => $groupName,
      "attribute_group_code" => $groupName,
      "status" => 1
    ]);

  return $groupId;
}

public function generateEntityJson()
{

  $entities = Entity::get()->toArray();
  foreach ($entities as $key => $entity) {
    $earr = [];
    $earr['marka'] = $entity['attributes']['marka'];
    foreach ($entity['attributes']['content'] as $akey => $avalue) {

      foreach ($avalue as $aakey => $aavalue) {
        if(is_array($aavalue['value'])) $aavalue['value'] = array_map('trim', $aavalue['value']);
        if(!is_array($aavalue['value'])) $aavalue['value'] = trim($aavalue['value']);
        $earr[Str::slug($aavalue['key'], '_')] = $aavalue['value'];
        print_r($key. "\n");

                    //id, type_id, key, value, ref_id, self_id
        if(!is_array($aavalue['value'])) {

          DB::table('JSONTESTFTS')->insert([
            'id' => $entity['entity_id'],
            'type_id' => 'meta',
            'key' => Str::slug($aavalue['key'], '_'),
            'value' => $aavalue['value']
          ]);
        }

      }

    }
           // DB::table('json_test')->insert(['id' => $entity['entity_id'], 'attr' => json_encode($earr)]);
  }

}

public function insertTermVariant($term_type_id, $term_name, $parent_id = 0, $level = 0, $path = '')
{
        //$parent_id = 0;
        //$level = 0;
        //$count = 0;
  $term = $this->insertTermValue($term_name);
  $termId = $term->term_id;

  $termTax = TermVariant::where('term_id', $termId)->where('term_type_id', $term_type_id)->first();
  if (!$termTax) {
    $termTax = TermVariant::create(['term_type_id' => $term_type_id, 'term_id' => $termId, 'parent_id' => $parent_id, 'level' => $level, 'path' => '']);
            //$termTax->save();
  }

  //$termTax->path = implode('/', $termTax->getPathText());
  //$termTax->save();

  return $termTax->variant_id;
}

public function insertTermValue($term_name)
{
  $term = Term::firstOrNew(['name' => $term_name, 'slug' => Str::slug($term_name)]);

  return $term;
}

public function insertBulkTermsEpey()
{
  $cats = DB::connection('epeyDetails')->table('details')->select(['cat', 'catSlug'])->orderBy('cat', 'asc')->pluck('cat','catSlug');

  foreach ($cats as $ckey => $cat) {
    print_r($ckey . '...'.PHP_EOL);


      $catName = trim($cat);
      //$brandName = trim($cat[1]);

      $this->insertTermVariant(29, $catName);
      //$this->insertTermVariant(15, $brandName);
    }

  return 0;
}

public function fixerAttrsCat()
{
  $cats = DB::connection('epeyDetails')->table('details')->select(['cat', 'catSlug'])->orderBy('cat', 'asc')->pluck('cat','catSlug');
//collect($attrs)->groupBy('metaName')->toArray();
  foreach ($cats as $ckey => $cat) {
    print_r($ckey . ':'.$cat.'...'.PHP_EOL);


DB::connection('epeyDetails')->table('attrs')->where('cat', $ckey)->update(['cat' => $cat]);
    }

  return 0;
}

public function insertBulkBrands($termTypeId = 0, $parent_id = 0)
{
  $brands = DB::connection('test02')->table('brands')->get();

  foreach ($brands as $key => $brand) {
    print_r($key . '...'.PHP_EOL);
    $this->insertTermVariant($termTypeId, $brand->name);
  }

  return 0;
}



public function setSpecCatsEpey($termTypeId = 0)

{


  //$specGroups = DB::connection('test02')->table('specGroups')->select('categoryName')->whereNotNull('categoryName')->groupBy('categoryName')->orderBy('categoryName', 'asc')->get();
  $specGroups = DB::connection('epeyDetails')->table('cats')->select('cat')->whereNotNull('cat')->groupBy('cat')->orderBy('cat', 'asc')->get();
    print_r($specGroups->count() . ': bulundu...'.PHP_EOL);
  foreach ($specGroups as $key => $spec) {
    print_r($key . '...'.PHP_EOL);
   $this->insertTermValue(explode('/', $spec->cat)[0]);
   $this->insertTermValue(explode('/', $spec->cat)[1]);
  /* $termId = $term->term_id;

   $termTax = TermVariant::where('term_id', $termId)->where('term_type_id', $termTypeId)->first();
   DB::connection('test02')->table('specGroups')->where('categoryName', $spec->categoryName)->update(['newCatId' => $termTax->variant_id]);*/
 }



 return 0;
}

}
