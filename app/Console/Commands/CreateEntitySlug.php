<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use SimpleXMLElement;
use Illuminate\Support\Facades\DB;
use App\Models\Content;
use App\Models\ContentIndex;
use App\Models\EntryType;
use App\Models\Entity;
use App\Models\TermEntity;
use App\Models\Term;
use App\Models\TermVariant;
use Illuminate\Support\Str;

/*
say: SELECT cat, count(cat) FROM epeyLinks GROUP BY cat ORDER BY count(cat) DESC



 */



class CreateEntitySlug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:create-slug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Entity içeriğine slug oluşturur.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
      parent::__construct();
  }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->generateContentSlug();
        print_r('bitti');
    }


    public function generateEntitySlug()
    {
        $entities = Entity::get();
        foreach ($entities as $key => $entity) {
            $entity->slug = Str::slug($entity->title);
            $entity->save();
            print_r(Str::slug($entity->title). "\n");

        }

    }
    public function generateEntitySlugIndex2()
    {
        $entities = ContentIndex::get();
        foreach ($entities as $key => $entity) {
            $attributes = $entity->attributes;
            $entity->permalink = Str::slug($attributes['title']);
            $entity->save();
            print_r($entity->permalink. PHP_EOL);

        }

    }
    public function generateEntitySlugIndex()
    {
        $entities = DB::table('contentIndex')->select(['content_id','title'])->whereNull('permalink')->orderBy('content_id')->get()->toArray();
        foreach ($entities as $key => $entity) {
            $permalink = Str::slug($entity->title);
            DB::table('contentIndex')->where('content_id', $entity->content_id)->limit(1)->update(['permalink' => $permalink]);
            print_r($key.':'.$permalink. PHP_EOL);

        }

    }

     public function generateContentSlug()
    {
        $entities = Content::where('entity_type_id', 30)
        //->limit(100)
        ->get();
        foreach ($entities as $key => $entity) {

            $attributes = $entity->attributes;
            $permalink = Str::slug($attributes['title']);
            $attributes['slug'] = $permalink;
            $entity->attributes = $attributes;
            $entity->save();
            //Content::where('content_id', $entity->content_id)->update(['attributes->slug' => $permalink]);

            print_r($entity->content_id.':'.$permalink. PHP_EOL);

        }

    }

}
