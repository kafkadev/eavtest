<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use SimpleXMLElement;
use Illuminate\Support\Facades\DB;
use App\Models\Content;
use App\Models\EntryType;
use App\Models\Entity;
use App\Models\TermEntity;
use App\Models\Term;
use App\Models\TermVariant;
use Illuminate\Support\Str;

class CreateEntityMetaJsonTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:json-test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Entity içeriğine json meta oluşturur.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        print_r('bitti');
        $this->generateEntityJson2();
    }

    public function generateEntityJson2()
    {

        $entities = Entity::get()->toArray();
        $floayTypes = ['Kg', 'GB', 'g', 'gün', 'MB', 'sa', 'inç', 'ghz', 'mhz', '%', 'mbps', 'W/kg', 'Wh', 'Mb', 'Gb', 'Μm Piksel', 'Gün', 'Ghz', 'Dk', 'Hz', 'mm', 'Mm', 'Çeyrek', 'Adet', 'nm', 'w', 'tb', 'mah', 'ppi', 'milyon', 'çekirdek', 'gb', 'gram', 'fps', 'saat', 'cm²'];

        foreach ($entities as $key => $entity) {
            $earr = [];
            $earr['marka'] = $entity['attributes']['marka'];

            foreach ($entity['attributes']['content'] as $akey => $avalue) {

                foreach ($avalue as $aakey => $aavalue) {
                    if (is_array($aavalue['value'])) {
                        $aavalue['value'] = array_map('trim', $aavalue['value']);
                    }

                    if (!is_array($aavalue['value'])) {
                        $aavalue['value'] = trim($aavalue['value']);
                    }

                    if (in_array($aavalue['value'], ['Var','Yok'])) {
                        $aavalue['value'] = $aavalue['value'];
                    }

                    $fieldKey = Str::slug($aavalue['key'], '_');
                    $fieldValue = $aavalue['value'];

                    if (!is_array($fieldValue)) {
                        $expValue = explode(' ', $fieldValue);
                        if ((count($expValue) == 2 && in_array($expValue[1], $floayTypes)) || is_numeric($expValue[0])) {
                            $fieldValue = floatval($expValue[0]);
                        }
                    }

                    $earr[$fieldKey] = $fieldValue;
                    print_r($fieldKey . "\n");

                    //id, type_id, key, value, ref_id, self_id
                    /*  if(!is_array($aavalue['value'])) {

                DB::table('JSONTESTFTS')->insert([
                'id' => $entity['entity_id'],
                'type_id' => 'meta',
                'key' => Str::slug($aavalue['key'], '_'),
                'value' => $aavalue['value']
                ]);
                }*/

                }

            }
            //DB::table('json_test2')->insert(['id' => $entity['entity_id'], 'attr' => json_encode($earr)]);
        }

    }

}
