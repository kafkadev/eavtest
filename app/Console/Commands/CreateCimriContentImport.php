<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use Illuminate\Support\Facades\DB;
use SimpleXMLElement;
use Illuminate\Support\Str;
use App\Models\Content;
use App\Models\EntryType;
use App\Models\Entity;
use App\Models\TermEntity;
use App\Models\Term;
use App\Models\TermVariant;
use App\Models\EavAttribute;
use App\Models\ContentMeta;
use App\Models\EavGroup;
use App\Models\Entities;


class CreateCimriContentImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:create-cimri-content-import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Entity içeriğine json meta value oluşturur.';
    protected $entityTypeId = 30;
    protected $termTypeId = 29;
    protected $exampleEntity = '{
      "title": "",
      "description": "",
      "entity_status": "draft",
      "open_comment": 0,
      "open_shareble": 1,
      "publish_date": "0000-00-00",
      "category": "",
      "created_at": 0,
      "creator_id": 1,
      "entity_type_id": 30,
      "parent_id": 0,
      "updated_at": "0"
    }';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
      parent::__construct();
      $this->exampleEntity = json_decode($this->exampleEntity, 1);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


      $this->generateContentCimri();
      print_r('bitti');
    }


    public function generateContentCimri()
    {

      $details = DB::connection('cimriDetails')->table('details')->select('id')
      //->limit(10)
      ->pluck('id');
     //$cats = collect($details)->groupBy('newCatId')->toArray();


      foreach ($details as $dkey => $di) {
        print_r($dkey.PHP_EOL);
        $detail = DB::connection('cimriDetails')->table('details')
        //->select('*')
        //->where('id', $di)
        ->find($di);
        $content = $this->exampleEntity;
        $data = json_decode($detail->data, 1);
        $prop = $data['props']['pageProps'];
        if (isset($prop['product'])) {
        $content['title'] = Str::title($prop['product']['product']['title']);
        $content['model'] = Str::title($prop['product']['product']['model']);
        $content['reviewInfo'] = $prop['product']['reviewInfo'];
        $content['prices'] = $prop['product']['prices'];
        $content['oldSource'] = $data['query']['route']['product'];
        $content['content'] = $prop['seoBullets']['content'];
        $content['description'] = $prop['review']['description'];
        $content['images'] = $prop['product']['imageIds'];
        $category = explode('/', $prop['product']['metaData']['fullCategory']);
        $content['category'] = end($category);
        $content['brand'] = $prop['brand']['name'];
        $specGroups = $prop['specs'][0]['specGroups'];
        if ($specGroups) {
          $specEntries = array_merge([], ...array_column($specGroups,'specEntries'));
          $specEntries = array_map(function($item){
            $item['name'] = Str::slug($item['name'],'_');
            return $item;
          }, $specEntries);
          $specItems = array_combine(array_column($specEntries, 'name'), array_column($specEntries, 'value'));
          $content = array_merge([], $content, $specItems);

          //print_r($content);
        }
        $collection = collect($content);
        $contentAttr = $collection->only(['entity_status', 'entity_type_id', 'parent_id', 'created_at', 'updated_at', 'creator_id']);

        $entityContentId = Content::insertGetId($contentAttr->toArray());
        $entityContent   = ContentMeta::create(['content_id' => $entityContentId, 'attributes' => $collection->toArray()]);

      }
      }

    }



  }
