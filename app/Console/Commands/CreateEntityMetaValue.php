<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPHtmlParser\Dom;
use SimpleXMLElement;
use Illuminate\Support\Facades\DB;
use App\Models\Content;
use App\Models\EntryType;
use App\Models\Entity;
use App\Models\TermEntity;
use App\Models\Term;
use App\Models\TermVariant;
use Illuminate\Support\Str;

/*
say: SELECT cat, count(cat) FROM epeyLinks GROUP BY cat ORDER BY count(cat) DESC



 */



class CreateEntityMetaValue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:create-meta-value';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Entity içeriğine json meta value oluşturur.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->generateEntityMetaValue();
      print_r('bitti');
    }


    public function generateEntityMetaValue()
    {

      $entities = DB::connection('liberyen')->table('epeyContent')->select('*')->get();
      $entities = collect($entities)->toArray();
      foreach ($entities as $key => $entity) {
        $earr = [];

        $entity->attributes = json_decode($entity->attributes,1);
        foreach ($entity->attributes['content'] as $key => $groupItems) {

          $groupName = $key;

          foreach ($groupItems as $gkey => $gvalue) {

            $optionName = $gvalue['key'];

            if (is_array($gvalue['value'])) {

              foreach ($gvalue['value'] as $keyo => $value) {
               $valueName = $value;
               $options = $this->insertOption($groupName, $optionName, $valueName, $entity->entity_type_id);

                         /*   DB::table('meta')->insert([
                                'entity_id' => $entity['entity_id'],
                                'meta_option_id' => $options[1],
                                'meta_option_value_id' => $options[2],
                                'meta_group' => $options[0]
                              ]);*/
                            }
                          } else {

                            $valueName = $gvalue['value'];
                            $options = $this->insertOption($groupName, $optionName, $valueName, $entity->entity_type_id);

                       /* DB::table('meta')->insert([
                            'entity_id' => $entity['entity_id'],
                            'meta_option_id' => $options[1],
                            'meta_option_value_id' => $options[2],
                            'meta_group' => $options[0]
                          ]);*/

                        }


                      }
                    }
                  }

                }

                public function insertOption($groupName, $optionName, $metaValue, $entityTypeId)
                {

                  $groupId = 0;
                  $group = DB::table('meta_groups')->where('meta_group_key', Str::slug(trim($groupName), '_'))->first();
                  if ($group) {
                    $groupId = $group->meta_group_id;
                  } else {
                    $groupId = DB::table('meta_groups')->insertGetId([
                      'meta_group_key' => Str::slug(trim($groupName), '_'),
                      'meta_group_label' => Str::title(trim($groupName)),
                      'entity_type_id' => $entityTypeId,
                    ]);
                  }


                  $optionId = 0;
                  $option = DB::table('meta_options')->where('meta_key', Str::slug(trim($optionName), '_'))->first();
                  if ($option) {
                    $optionId = $option->meta_option_id;
                  } else {
                    $optionId = DB::table('meta_options')->insertGetId([
                      'meta_key' => Str::slug(trim($optionName), '_'),
                      'default_label' => Str::title(trim($optionName)),
                      'meta_group_id' => $groupId
                    ]);
                  }



                  $valueId = 0;
                  $metaValues = DB::table('meta_option_values')
                  ->where('meta_option_id', $optionId)
                  ->where('meta_option_value', Str::title(trim($metaValue)))
                  ->first();
                  if ($metaValues) {
                    $valueId = $metaValues->meta_option_value_id;
                  } else {
                    $valueId = DB::table('meta_option_values')->insertGetId([
                      'meta_option_id' => $optionId,
                      'meta_option_value' => Str::title(trim($metaValue)),
                    ]);
                  }



                  return [$groupId, $optionId, $valueId];
                }


                public function generateBrand()
                {
                  $termId     = 0;
                  $collection = DB::table('entity2')->get();
                  foreach ($collection as $key => $value) {
                    $content   = json_decode($value->attributes);
                    $termName  = $content->marka;
                    $termTaxId = $this->insertTerm($termName, 15);
            //DB::table('term_entity')->updateOrInsert(['entity_id' => $value->entity_id, 'variant_id' => $termTaxId], ['entity_id' => $value->entity_id, 'variant_id' => $termTaxId]);

                  }
                }

                public function insertTerm($termName, $type_id)
                {
                  $termTaxId = 0;
                  $term      = DB::table('terms')->select('term_id')->where('name', $termName)->first();
                  if (!$term) {
                    $termId = DB::table('terms')->insertGetId(['name' => $termName, 'slug' => $termName]);
                  } else {
                    $termId = $term->term_id;

                  }
                  $termTax = DB::table('term_variant')->where('term_id', $termId)->where('term_type_id', $type_id)->first();
                  if ($termTax) {
                    $termTaxId = $termTax->variant_id;
                  } else {
                    $termTaxId = DB::table('term_variant')->insertGetId(['term_id' => $termId, 'term_type_id' => $type_id]);
                  }
                  return $termTaxId;
                }
                public function addGroup(Request $request)
                {
                  $data = ["location_obj" => $request->input('location_obj')];


                  $attributes = $request->input('attributes');


                  $item = EavGroup::create([
                    'attribute_group_name' => "",
                    'attribute_group_code' => "",
                    'tab_group_code' => "",
                    'description' => "",
                    'rules' => "",
                    'taxonomy' => "",
                    'term_taxonomy_ids' => "",
                    'sort_order' => "",
                    'status' => "",
                    'is_primary' => "",
                    'data' => json_encode($data),
                  ]);
                  return response()->json($item);
                }

                public function insertTermVariant($term_type_id, $term_name, $parent_id = 0, $level = 0, $path = '')
                {
        //$parent_id = 0;
        //$level = 0;
        //$count = 0;
                  $term = $this->insertTermValue($term_name);
                  $termId = $term->term_id;

                  $termTax = TermVariant::where('term_id', $termId)->where('term_type_id', $term_type_id)->first();
                  if (!$termTax) {
                    $termTax = TermVariant::create(['term_type_id' => $term_type_id, 'term_id' => $termId, 'parent_id' => $parent_id, 'level' => $level, 'path' => '']);
            //$termTax->save();
                  } else {
                    $termTax->path = implode('/', $termTax->getPathText());
                    $termTax->save();
                  }
                  return $termTax->variant_id;
                }

              }
