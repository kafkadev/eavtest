<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
               // \App\Console\Commands\CreateEntityMetaValue::class,
                \App\Console\Commands\CreateEntityGroupMetaValue::class,
                \App\Console\Commands\CreateCimriContentImport::class,
                \App\Console\Commands\CreateEntityContentImport::class,
                \App\Console\Commands\CreateBulkTerms::class,
                \App\Console\Commands\CreateEntitySlug::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
