<?php
use Illuminate\Support\Facades\DB;
use App\Models\Content;
use App\Models\EntryType;
use App\Models\Entity;
use App\Models\TermEntity;
use App\Models\Term;
use App\Models\TermVariant;
use Illuminate\Support\Str;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/{term_slug}/{entity_slug}', function ($term_slug,$entity_slug) {

    return response()->json([$term_slug,$entity_slug]);
});


$router->get('/{term_slug}/{entity_slug}-i{entity_item_id}', function ($term_slug,$entity_slug,$entity_item_id) {

    return response()->json([$term_slug,$entity_slug,$entity_item_id]);
});

$router->get('/{last_level_term_slug}/{entity_slug}-i{entity_item_id}', function ($last_level_term_slug,$entity_slug,$entity_item_id) {

    return response()->json([$last_level_term_slug,$entity_slug,$entity_item_id]);
});


$router->get('/blog/{entity_slug}-i{entity_item_id}', function ($last_level_term_slug,$entity_slug,$entity_item_id) {

    return response()->json([$last_level_term_slug,$entity_slug,$entity_item_id]);
});

$router->get('/page/{entity_slug}-i{entity_item_id}', function ($last_level_term_slug,$entity_slug,$entity_item_id) {

    return response()->json([$last_level_term_slug,$entity_slug,$entity_item_id]);
});


$router->get('/{term_slug}-t{term_type_id}-i{term_item_id}/{entity_slug}-e{entity_type_id}-i{entity_item_id}', function ($term_slug,$term_type_id,$term_item_id,$entity_slug,$entity_type_id,$entity_item_id) {
    $entry_type = EntryType::findOrFail($term_type_id);
    $response = [$entry_type];
    if($entry_type->property_code == 'term'){
        $response[] = TermVariant::where('variant_id', $term_item_id)->with(['term'])->where('term_type_id', $term_type_id)->first();
    if (EntryType::findOrFail($entity_type_id)->property_code == 'entity') {
        $response[] = Entity::find($entity_item_id);
    }
    }
    return response()->json([$response,$term_slug,$term_type_id,$term_item_id,$entity_slug,$entity_type_id,$entity_item_id]);
});