<?php

use Illuminate\Http\Request;
use App\Models\JsonTest;
use Illuminate\Support\Facades\DB;
use App\Models\Content;
use Illuminate\Support\Facades\Cache;
//use SQLite3;

$router->get('/clientInit', function (Request $request) {

    $cacheKey = 'clientInit';

    if (Cache::has($cacheKey)) {
        $response = Cache::get($cacheKey);
    } else {


    $mainCategories  = DB::table('term_variant')->select('*')
        ->leftJoin('terms', 'term_variant.term_id', '=', 'terms.term_id')
        ->where('term_variant.term_type_id', 28)
        ->where('term_variant.parent_id', 0)
        ->get()->toArray();


        $bestContents = Content::select('*')
            ->where('entity_type_id', 30)
            ->inRandomOrder()
            ->limit(48)
            ->get()
            ->toArray();

    $response = [
        "mainCategories" =>$mainCategories,
        "bestContents" =>$bestContents,
    ];

    Cache::put($cacheKey, $response, (3600*24));
    }
    return response()->json($response);
});


$router->get('/cacheDelete', function (Request $request) {
    Cache::flush();
});











$router->get('/sitemap_products.xml', function () use ($router) {
    $links = DB::table('contentIndex')->select("*")
    //->where('entity_type_id', 30)
    ->limit(5000)->get()->toArray();
    /*$links = DB::table('contents')->select(['created_at','content_id', DB::raw("json_extract(attributes, '$.slug') as slug") ])
    ->where('entity_type_id', 30)
    ->limit(5000)->get()->toArray();*/
    return response(view('sitemap', ['links' => $links]), 200)->header('Content-Type', 'application/xml');
});



$router->get('/sitemap_categories.xml', function () use ($router) {
    $links = DB::table('term_variant')->select(['variant_id','term_type_id'])
    ->where('term_type_id', 28)->get()->toArray();
    return response(view('sitemap_category', ['links' => $links]), 200)->header('Content-Type', 'application/xml');
});


$router->get('register', function () {
    return view('welcome');
});

$router->group(['middleware' => 'auth:api'], function () use ($router) {
$router->get('/user', function (Request $request) {
    return $request->user();
});
});


$router->get('/entities', 'EntityController@getEntities');
$router->get('/last10', 'EntityController@getLastEntity');


$router->group(['middleware' => 'auth'], function () use ($router) {

  $router->post('/add_content/{entity_type_id}', 'ContentController@addContent');

});
$router->get('/contents/{entity_type_id}', 'ContentController@getContents');
$router->get('/content/{content_id}', 'ContentController@getContent');
$router->get('/frontcontent/{content_id}', function (Request $request, $content_id) {
    $res = file_get_contents(__DIR__.'/demoResponseItem.json');
    return response()->json(json_decode($res, 1));
});


$router->get('/attrbute_group_demo', 'AttributeController@addGroup');
$router->post('/add_group/{entity_id}', function (Request $request, $entityId) {

    //$request->except(['attribute_fields']);
        $request->merge([
            'entity_type_id'       => $entityId,
            'places'              => json_encode($request->places),
            'selected_attributes' => json_encode($request->selected_attributes),
            'attribute_fields'    => json_encode($request->attribute_fields),
        ]);

        $where = [];

        if ($request->attribute_group_id) {
            $where['attribute_group_id'] = $request->attribute_group_id;
        } else {
            $where = [
                'entity_type_id'       => $entityId,
                'attribute_group_code' => $request->attribute_group_code,
            ];

        }
        DB::table('eav_attribute_group')->updateOrInsert($where, $request->except(['attribute_fields', 'data', 'hide', 'attribute_group_id']));
        //EavGroup::create($request->all());
        /* DB::table('eav_attribute_group')->where('attribute_group_id', 2)->update([
        'data' => json_encode($data)
        ]);*/
        /*DB::table('eav_attribute_group')->create([
        'attribute_group_name' => "",
        'attribute_group_code' => "",
        'tab_group_code' => "",
        'description' => "",
        'rules' => "",
        'taxonomy' => "",
        'term_taxonomy_ids' => "",
        'sort_order' => "",
        'status' => "",
        'is_primary' => "",
        'data' => json_encode($data),
        ]);*/

        // $entities = DB::table('eav_attribute_group')->get();
        return response()->json($request->all());
    });
$router->post('/update_attribute', 'AttributeController@updateAttribute');
$router->post('/attach_attr', 'AttributeController@attachAttrToGroup');
$router->post('/add_attribute/{entity_id}', 'AttributeController@addAttribute');
$router->post('/add_taxonomy', 'TermsController@addTaxonomy');
$router->post('/add_reply', 'AttributeController@addContent');


$router->get('/getSearch', 'FormController@getSearch');
$router->get('/getCrudForm', 'FormController@getCrudForm');
$router->get('/getSearchGroup', 'FormController@getSearchGroup');
$router->get('/getFilterableFields', 'FormController@getFilterableFields');




$router->get('/jsonTest', function (Request $request) {

    $res = JsonTest::limit(10)->get()->toArray();
   return response()->json($res);

  });


$router->get('/offerSearchPDO', function (Request $request) {
//SELECT * FROM PostSearch WHERE PostSearch MATCH 'title:Apple OR Iphone Xs OR 64 OR Gb OR İnç OR Çift OR Hatlı OR 12 OR Mp OR Akıllı OR Cep OR Telefonu OR Gümüş'  order by rank  limit 10;
$stringArr = explode(' ', mb_strtolower($request->text));
$stringArr = array_map(function($text){
        if (preg_match('/[^a-z0-9]+/i',$text)) {
        return '"'.$text.'"';
    } else {
        return $text;

    }

}, $stringArr);
$queryText = $stringArr[0] . ' AND ' . $stringArr[1]
//. ' AND ' . $stringArr[2]
. ' AND ('. implode(' OR ', $stringArr).')';

/*print_r($queryText);
die();*/

    $testjson = DB::connection('ftsSearch')->table('PostSearch')->select('offers.*')
    ->leftJoin('offers', 'offers.id', '=', 'PostSearch.id')
    ->whereRaw("PostSearch MATCH 'title:$queryText' group by offers.title order by PostSearch.rank")
    ->limit(10)->get();
    return response()->json($testjson ? $testjson : 0);

  });



$router->get('/offerSearch', function (Request $request) {


    $request->text = strtolower($request->text);

//SELECT * FROM PostSearch WHERE PostSearch MATCH 'title:Apple OR Iphone Xs OR 64 OR Gb OR İnç OR Çift OR Hatlı OR 12 OR Mp OR Akıllı OR Cep OR Telefonu OR Gümüş'  order by rank  limit 10;
$stringArr = explode(' ', mb_strtolower($request->text));
$stringArr = array_map(function($text){
        if (preg_match('/[^a-z0-9]+/i',$text)) {
        return '"'.$text.'"';
    } else {
        return $text;

    }

}, $stringArr);

$stringArr = array_filter($stringArr, function($v, $k) {
    return strlen($v) > 2;
}, ARRAY_FILTER_USE_BOTH);


$queryText = $stringArr[0] 
//. ' AND ' . $stringArr[1]
//. ' AND ' . $stringArr[2]
. ' AND ('. implode(' OR ', $stringArr).')';

/*print_r($queryText);
die();*/

$db = new \SQLite3(database_path('db/offers.db'));
$ret = $db->query("SELECT offers.* FROM PostSearch left join offers on offers.id = PostSearch.id WHERE PostSearch MATCH 'title:$queryText' group by offers.title order by PostSearch.rank  limit 10");

   $projects = [];
     while ($row = $ret->fetchArray(SQLITE3_ASSOC)) {
            $projects[] = $row;
        }

$db->close();



    return response()->json($projects ? $projects : []);

  });

$router->get('/jsonTest/{id}', function (Request $request, $id) {

    $res = JsonTest::find($id)->toArray();
   return response()->json($res);

  });











$router->get('/terms', 'TermsController@getTerms');
$router->get('/terms/{type_id}', 'TermsController@getTermsWithType');
$router->get('/terms/{type_id}/{catId}', 'TermsController@getTermsWithTypeSub');

require_once('user.php');
require_once('temp.php');
require_once('backup.php');
