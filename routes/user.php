<?php
  $router->post('/getLogin', 'UserController@getLogin');
  $router->group(['middleware' => 'auth'], function () use ($router) {
      $router->get('/my', 'UserController@getMyDetail');
      $router->get('/getUsers', 'UserController@getUsers');
      $router->get('/getRoles', 'UserController@getRoles');
      $router->get('/getUser/{user_id}', 'UserController@getUser');
  });