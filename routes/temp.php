<?php
use App\Models\Content;
use App\Models\ContentMeta;
use App\Models\EavGroup;
use App\Models\Entity;
use App\Models\Media;
use App\Models\Term;
use App\Models\TermEntity;
use App\Models\TermVariant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

$router->get('/categoryList/{catName}', function ($catName) {
    $catName = htmlspecialchars(urldecode($catName));
    $catIds  = DB::table('term_variant')->select('name')
        ->leftJoin('terms', 'term_variant.term_id', '=', 'terms.term_id')
        ->where('term_variant.term_type_id', 28)
        ->whereRaw("term_variant.path glob '*/$catName*' or term_variant.path glob '$catName/*'")->pluck('name')->toArray();
   /* $catIds = array_map(function ($item) {
        $item = ' ' . $item;
        return $item;
    }, $catIds);*/

    //content içerisi category id olarak belirtilecek
    $testjson = DB::table('contentIndex')->select("*")
    //$testjson = Content::select("*")
    //$testjson = Content::select(['content_id', 'attributes->category as category', 'attributes->brand as brand', 'attributes->title as title', 'attributes->images as images', 'attributes->slug as slug', 'attributes->model as model', 'attributes->prices->minPrice as minPrice', 'attributes->prices->maxPrice as maxPrice'])
    //->setJoinMeta()
        //->where('entity_type_id', 30)
        ->whereIn("category", array_map("trim", $catIds))->limit(30)->get()->toArray();

        $testjson = array_map(function ($item) {
            $item->images = json_decode($item->images, 1);
            return $item;
        }, $testjson);



    return response()->json($testjson);
});

$router->get('/contentItem/{itemId}', function ($itemId) {

    $testjson = Content::where('content_id', $itemId)->first();
    $cat      = TermVariant::where('term_type_id', 28)->setJoinTerm()
        ->where('name', trim($testjson->attributes['category']))
        ->first();
    $catGroups = 0;
    if ($cat) {
        $catGroups = EavGroup::select(['attribute_group_id', 'attribute_group_code', 'attribute_group_name'])
            ->where('entity_type_id', 30)
        //->where('selected_terms', 'like', "%$cat->variant_id%")
            ->where("selected_term", $cat->variant_id)
        //->whereIn("selected_term", [$cat->variant_id])
            ->get()->toArray();
    }

/* BU BÖLÜM İÇİN CACHE VEYA BENZERİ KULLANILABİLİ */

    return response()->json([
        'cat'       => $cat,
        'item'      => $testjson,
        'catGroups' => $catGroups,
    ]);
});

$router->get('/catWithContents/{catName}', function ($catName) {

    $testjson = Content::select('contents.*')->setJoinIndex()
        ->where('contents.entity_type_id', 30)
        ->whereRaw("json_extract(contentIndex.attributes, '$.category') = ' Cep Telefonları'")->limit(10)->get();
    return response()->json($testjson ? $testjson : 0);
});

$router->get('/categoryList3/{catName}', function ($catName) {
    $catName = htmlspecialchars(urldecode($catName));
    $catIds  = DB::table('term_variant')->select('name')
        ->leftJoin('terms', 'term_variant.term_id', '=', 'terms.term_id')
        ->where('term_variant.term_type_id', 28)
        ->whereRaw("term_variant.path glob '*/$catName*' or term_variant.path glob '$catName/*'")->pluck('name');

    $testjson = DB::table('contentIndex')
        ->where('entity_type_id', 30)
        ->whereIn('category', $catIds)
    //->whereRaw("attributes, '$.category') = ' $catName'")
        ->limit(50)
        ->get()->toArray();
    // print_r(htmlspecialchars(urldecode($catName)));
    $testjson = array_map(function ($item) {
        $item->attributes = json_decode($item->attributes, 1);
        return $item;
    }, $testjson);
/*
$test02 = Cache::remember('test02'.$catName, 3600, function () use($catIds){
return DB::table(DB::raw('contentIndex, json_tree(attributes)'))->selectRaw("json_tree.key key, json_tree.atom atom, json_tree.type type, count(json_tree.atom) count")
->whereIn('category', $catIds)
->whereRaw("brand = 'Samsung' and json_tree.type = 'text' and length(json_tree.atom) < 40 and json_tree.atom GLOB '[^0-9]*' group by key, atom order by count desc")->get()->toArray();
});
 */

    return response()->json($testjson ? $testjson : 0);
});

$router->get('/indexItem/{itemId}', function ($itemId) {

    $testjson             = DB::table('contentIndex')->where('content_id', $itemId)->first();
    $testjson->attributes = json_decode($testjson->attributes, 1);
    $cat                  = TermVariant::where('term_type_id', 28)->setJoinTerm()
        ->where('name', $testjson->category)
        ->first();
    $jsonTest = DB::table(DB::raw("eav_attribute_group, json_each(eav_attribute_group.places, '$.terms') json_each"))
        ->select(DB::raw('eav_attribute_group.attribute_group_id'))
        ->where([['entity_type_id', '=', 30], ['is_primary', '=', 0]])->whereIn("json_each.value", array_map('intval', [$cat->variant_id]))->get()->pluck('attribute_group_id');
    $catGroups = EavGroup::select(['attribute_group_id', 'attribute_group_code', 'attribute_group_name'])->where('entity_type_id', 30)->whereIn("attribute_group_id", $jsonTest)->get()->toArray();

    return response()->json(['cat' => $cat, 'item' => $testjson, 'catGroups' => $catGroups]);
});
$router->get('/categoryList4/{catName}', function ($catName) {
    $catName  = htmlspecialchars(urldecode($catName));
    $testjson = ContentMeta::select('content_meta.*')->setJoinContent()
        ->where('contents.entity_type_id', 30)
        ->whereRaw("json_extract(content_meta.attributes, '$.category') = ' $catName'")->limit(10)->get()->toArray();
    // print_r(htmlspecialchars(urldecode($catName)));
    return response()->json($testjson ? $testjson : $catName);
});

$router->get('/categoryList2/{catName}', function ($catName) {

    $testjson = DB::table('content_meta')->select('content_meta.*')
        ->leftJoin('contents', 'content_meta.content_id', '=', 'contents.content_id')
        ->where('contents.entity_type_id', 30)
        ->whereRaw("json_extract(content_meta.attributes, '$.category') = ' Cep Telefonları'")->limit(10)->get();
    return response()->json($testjson ? $testjson : 0);
});

$router->get('/hostTest', function () {

    return DB::connection('prod')->table(DB::raw('contentIndex'))->select("*")
        ->whereRaw("json_extract(attributes, '$.brand') = 'Samsung'")->get()->toArray();
});

$router->get('/add-wpposts', function (Request $request) {
    $db    = DB::connection('sqlitewp');
    $items = $db->table('wp_posts')->limit(10)->get()->map(function ($item) {
        return [
            'title'   => $item->post_title,
            'content' => $item->post_content,
        ];
    });
    return $items;
});

$router->get('/files', function () {
    $fileDetail = [
        'reference_id'   => 9999999,
        'entity_type_id' => 9999999,
        'file_name'      => "dosya.png",
        'storage_name'   => "general",
        'data'           => ['deneme' => 12],
    ];
    Media::create($fileDetail);
});
$router->get('/search', function () {
    //$request = app('request')->all();
    $app = app();
    /*$jsonTest = DB::table('entity')
    ->selectRaw("JSON_EXTRACT(attributes, '$.marka') as marka, count(JSON_EXTRACT(attributes, '$.marka')) count")
    ->groupBy('marka')
    ->get();
     */

    $jsonTest = EavGroup::select('*')
    //$jsonTest = JsonTable::select('*')
    //$jsonTest = DB::connection('maridbtest')->table('json_test')
    //->select('attr->marka as marka', DB::raw('count(id) count'))
        ->whereRaw("JSON_CONTAINS(places, '[\"article\"]', 'entities')")
    //->whereJsonContains('data->places->entities', ['article'])
    //->whereJsonContains('attr->ekran_ozellikleri', ['Safir Kristal', 'Çizilmeye Dirençli','Gece Görüş Modu'])
    //->whereJsonContains('attr->desteklenen_aktiviteler', ['Yüzme', 'Golf'])
    //->where('attributes->marka', 'Acer')
    //->where('attributes->kod', '(UM.PX1EE.005)')
    //->groupBy('marka')
        ->get();
    /*
    $jsonTest = JsonTable::where('attr->mikrofon', 'Var')
    ->where('attr->marka', app('request')->input('marka'))
    //->where('attr->ekran_teknolojisi', 'AMOLED')
    //->where('attr->govde_malzemesi', 'Alüminyum')
    //->groupBy('mikrofon')
    ->limit(1)
    ->first();
     */
    //$jsonTest->attr = json_decode($jsonTest->attr,1);
    //return response()->json(app('router')->getRoutes());
    return response()->json($jsonTest);
});

$router->get('/mariadb-json-test', function () {
    $request  = app('request');
    $jsonTest = DB::connection('maridbtest')->table('json_test')
    //->where('attr->govde_malzemesi', 'Alüminyum')
    //->whereJsonContains('attr->kordon_malzemesi', ['Naylon','Örgü'])
    //->selectRaw("JSON_EXTRACT(attr, '$.marka') as marka, count(JSON_EXTRACT(attr, '$.marka')) count")
    //->select('attr->marka as marka', DB::raw('count(id) count'))
    //->whereJsonContains('attr->ekran_teknolojisi', ['AMOLED'])
    //->whereJsonContains('attr->servis_ve_uygulamalar', ['Gelen SMS Bildirimi'])
    //->orWhereJsonContains('attr->desteklenen_aktiviteler', ['Golf'])
    //->where('attr->marka', $request->input('marka'))
    //->where('attr->ekran_teknolojisi', 'AMOLED')
    //->where('attr->govde_malzemesi', 'Alüminyum')
        ->where('attr->marka', 'Acer')
        ->where('attr->yil', 2015)
    //->where('attr->kod', '(UM.PX1EE.005)')
    //->groupBy('marka')
        ->limit('10')
        ->get();
    return response()->json($jsonTest);
});

$router->get('/mariadb-json-search', function () {
    $request  = app('request');
    $jsonTest = DB::connection('maridbtest')->table('json_test')
    //->where('attr->govde_malzemesi', 'Alüminyum')
    //->whereJsonContains('attr->kordon_malzemesi', ['Naylon','Örgü'])
    //->selectRaw("JSON_EXTRACT(attr, '$.marka') as marka, count(JSON_EXTRACT(attr, '$.marka')) count")
    //->select('attr->marka as marka', DB::raw('count(id) count'))
    //->whereJsonContains('attr->ekran_teknolojisi', ['AMOLED'])
    //->whereJsonContains('attr->servis_ve_uygulamalar', ['Gelen SMS Bildirimi'])
    //->orWhereJsonContains('attr->desteklenen_aktiviteler', ['Golf'])
    //->where('attr->marka', $request->input('marka'))
    //->where('attr->ekran_teknolojisi', 'AMOLED')
    //->where('attr->govde_malzemesi', 'Alüminyum')
        ->where('attr->marka', 'Acer')
        ->where('attr->yil', 2015)
    //->where('attr->kod', '(UM.PX1EE.005)')
    //->groupBy('marka')
        ->limit('10')
        ->get();
    return response()->json($jsonTest);
});

$router->get('/mongodb', function (Request $request) {
    $products       = DB::connection('mongodb')->collection('products');
    $jsonTableItems = DB::table('json_test')->limit(10)->get();
    foreach ($jsonTableItems as $key => $value) {
        $attr = json_decode($value->attr, 1);
        $products->insert($attr);
    }
    return response()->json([]);
});

$router->get('/mongodb-get', function (Request $request) {
    $products = DB::connection('mongodb')->collection('products');
    $products = $products->where('ekran_ozellikleri', 'Çizilmeye Dirençli')->limit(10)->get();
    return response()->json($products);
});

$router->get('/', function () {

    /* \Eav\Attribute::add([
    'attribute_code' => 'size',
    'entity_code' => 'product',
    'backend_class' => null,
    'backend_type' => 'int',
    'backend_table' =>  null,
    'frontend_class' =>  null,
    'frontend_type' => 'select', // Assgin the type "select"
    'frontend_label' => 'Size',
    'source_class' => null,
    'options' => [
    's' => 'Small',
    'm' => 'Medium',
    'l' => 'Large',
    'xl' => 'Xtra Large',
    ],
    'default_value' => 's',
    'is_required' => 0,
    'required_validate_class' =>  null
    ]);

    EavEntity::create([
    'deneme' => 'asdasdasda',
    'name' => 'Flamethrower',
    'sku'  => '1HJK92',
    'size'  => 's',
    'upc'  => 'SHNDUU451888',
    'status'  => "1",
    'description' => 'Not a Flamethrower',
    'search' => 1
    ]);

    $search = EavEntity::whereAttribute('upc', 'like', 'SHNDUU%')
    ->whereAttribute('sku', '=', '1HJK92')
    ->whereAttribute('name', '=', 'Flamethrower')
    ->select(['*','attr.*']);
    //$search = EavEntity::select(['*','sku','upc']);
    $result = $search->toSql();
    $facets = $search->getFacets(true);
    return [$result,$facets];

     */
    return 1;

});

$router->get('/init-data/{slug}', function ($slug) use ($router) {

    $testjson = Term::where('slug', $slug)
        ->setJoinTermVariant()
        ->where('term_variant.term_type_id', 15)
        ->limit(10)
        ->first()->termVariant()->first()->termEntity()->limit(5)->get();
    //$testjson = TermVariant::selectRaw("entity.title, entity.cat")
    //$testjson = TermVariant::selectRaw("group_concat(entity.entity_id) as ids")
    /*->where('term_variant.term_type_id', 15)
    //->where('term_variant.variant_id', 645)
    ->Where('terms.slug', $slug)
    ->setJoinTerm()
    ->setJoinTermEntity()
    ->setJoinEntity()
    ->limit(10)
    ->first();*/
    //->pluck('title');
    // $testjson = $testjson->childrenRecursive;
    return response()->json($testjson ? $testjson : 0);
});

$router->get('/init-data2/{slug}', function ($slug) use ($router) {
    $testjson = TermVariant::selectRaw("*")
        ->with(['children', 'term'])
    //$testjson = TermVariant::selectRaw("entity.title, entity.cat")
    //$testjson = TermVariant::selectRaw("group_concat(entity.entity_id) as ids")
    //->where('term_variant.term_type_id', 15)
    //->where('term_variant.variant_id', 645)
        ->where('terms.slug', $slug)
        ->setJoinTerm()
    //->setJoinTermEntity()
    //->setJoinEntity()
    //->limit(10)
        ->first();
    //->pluck('title');
    $breadCrumbs = $testjson->getParentItems();
    $entities    = $testjson->termEntity()->limit(10)->get();
    $response    = ['memory' => convert(memory_get_usage()), 'term' => $testjson, 'bread_crumb' => $breadCrumbs ?? 0, 'entities' => $entities ?? 0];
    return response()->json($response);
});

$router->get('/init-data3/{term_slug}/{entity_slug}', function ($term_slug, $entity_slug) use ($router) {
    $term = TermVariant::selectRaw("*")
    //->with(['parent'])
    //$testjson = TermVariant::selectRaw("entity.title, entity.cat")
    //$testjson = TermVariant::selectRaw("group_concat(entity.entity_id) as ids")
    //->where('term_variant.term_type_id', 15)
    //->where('term_variant.variant_id', 645)
        ->where('terms.slug', $term_slug)
        ->setJoinTerm()
    //->setJoinTermEntity()
    //->setJoinEntity()
    //->limit(10)
        ->first();
    //->pluck('title');
    $breadCrumbs = $term->getParentItems();
    $entities    = $term->termEntity()->where('slug', $entity_slug)->limit(10)->get();
    return response()->json([convert(memory_get_usage()), $breadCrumbs ?? 0, $entities ?? 0, $term]);
});

$router->get('/entry/{entity_id}', function ($entity_id) use ($router) {

    $entities = Entity::with('termList')->find($entity_id);

    return response()->json($entities);
});

$router->get('/entry/{entity_slug}.html', function ($entity_slug) use ($router) {

    $entities = Entity::where('slug', $entity_slug)->first();
    return response()->json($entities);
});
/*
$router->get('/entry/{entity_id}', function ($entity_id) use ($router) {

$entities = Entity::find($entity_id);
return response()->json($entities);
});*/

Route::get('register', function () {
    return 1;
});

Route::get('login', function () {
    return 1;
});
