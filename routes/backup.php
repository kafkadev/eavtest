<?php
use App\Models\EavAttribute;
use App\Models\EavGroup;
use App\Models\Entity;
use App\Models\EntityDemo;
use App\Models\Term;
use App\Models\TermEntity;
use App\Models\TermVariant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

$router->get('/endemo', function () {
    $entities = EntityDemo::create([
        'entity_type_id' => 1,
        'owner_id'       => 0,
        'parent_id'      => 0,
        'status'         => 0,
    ]);
    /*
    $entities = EntityDemo::where('entity_id',1257)->update([
    'entity_type_id' => 1,
    'owner_id' => 0,
    'parent_id' => 0,
    'status' => 0,
    //'created_at' => 0
    ]);
     */
    return response()->json($entities);
});



$router->get('/entity_types', function () {
    $entities = DB::table('mg_eav_entity_type')->get();
    return response()->json($entities);
});

$router->get('/attribute_group', function () {
    $entities = DB::table('eav_attribute_group')
        ->get();
    return response()->json($entities);
});
$router->get('/attribute_group/{group_id}', function ($group_id) {
    $group = EavGroup::where('attribute_group_id', $group_id)->first();
    $attributes = DB::table('eav_attribute')->where('entity_type_id', $group->entity_type_id)->where('attribute_group_id', $group->attribute_group_id)->get()->toArray();
    $group->attributes = $attributes;
    return response()->json($group);
});
$router->get('/attribute_set', function () {
    $entities = DB::table('eav_attribute_set')->get();
    return response()->json($entities);
});
$router->get('/attributes/{entity_id}', function ($entity_type_id) {
    $entities = DB::table('eav_attribute')->where('entity_type_id', $entity_type_id)->get();
    return response()->json($entities);
});
$router->get('/attributes', function () {
    $entities = DB::table('eav_attribute')->get();
    return response()->json($entities);
});
$router->get('/attribute/{attribute_id}', function ($attribute_id) {
    $attribute = EavAttribute::where('attribute_id', $attribute_id)->first();
    return response()->json($attribute);
});
$router->get('/entity/{entity_id}', function ($entity_id) {
    $groups         = EavGroup::where('entity_type_id', $entity_id)->get()->toArray();
    $attributes         = EavAttribute::where('entity_type_id', $entity_id)->where('attribute_group_id',0)->get()->toArray();
    $entity             = DB::table('entities')->where('id', $entity_id)->first();
    $entity->attributes = $attributes;
    $entity->groups = $groups;
    return response()->json($entity);
});
