<?php
use Illuminate\Support\Facades\DB;
use App\Models\Content;
use App\Models\EntryType;
use App\Models\Entity;
use App\Models\TermEntity;
use App\Models\Term;
use App\Models\TermVariant;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */

$router->get('/', function () use ($router) {
    return $router->app->version();
});



$router->get('/insertTerm', 'ExampleController@insertTermTest');
$router->get('/termEntityTest', 'ExampleController@termEntityTest');
$router->get('/entityTermsTest', 'ExampleController@entityTermsTest');
$router->get('/tests/fts565', function () {

    $testjson = DB::table('PostSearch')->select('*')->whereRaw("PostSearch MATCH 'term_slug:Garmin'")->get();
    return response()->json($testjson ? $testjson : 0);
});
$router->get('/termTest', function () {

    return response()->json($testjson ? $testjson : 0);
});

$router->get('/json_test/{ent_id}', function ($ent_id) {
    $entity = DB::table('json_test2')->find($ent_id);
    $entity = json_decode($entity->attr);
    return response()->json($entity);
});

$router->get('/{term_slug}/{entity_slug}-i-{entity_id}', function ($term_slug,$entity_slug,$entity_id) {


   /* $term = 0;*/
    $term = TermVariant::where('term_variant.term_type_id', 15)
    ->where('terms.slug', $term_slug)
    ->where('entity.entity_id', $entity_id)
    ->setJoinTerm()
    ->setJoinTermEntity()
    ->setJoinEntity()
    ->first()->termEntity()->with(['termList'])->first()->toArray();
    /*
    //->termEntity()->with(['termList'])->where('slug', $entity_slug)->first();
    //$term = Term::where('slug', $term_slug)->first();
    $entity = 0;
  $entity = Entity::with(['termList'])->find(4)->toArray();
  //$entity = Entity::with(['termList'])->where('slug', $entity_slug)->where('entity_id', $entity_id)->first();*/
 foreach ($term['attributes']['content'] as $key => $groupItems) {
      //print_r($groupItems);
      DB::table('meta_options')->insert([
        'meta_key' => Str::slug(trim($key), '_'),
        'default_label' => Str::title(trim($key))
        ]);
      foreach ($groupItems as $gkey => $gvalue) {
          $optionId = DB::table('meta_options')->insertGetId([
              'meta_key' => Str::slug(trim($gvalue['key']), '_'),
              'default_label' => Str::title(trim($gvalue['key']))
              ]);
              $type = gettype($gvalue['value']);
              if (is_array($gvalue['value'])) {
                  foreach ($gvalue['value'] as $keyo => $value) {
                   $valueId = DB::table('meta_option_values')->insertGetId([
                        'meta_option_id' => $optionId,
                        'meta_option_value' => Str::title(trim($value)),
                        ]);

                        DB::table('meta_array')->insert([
                            'entity_id' => $term['entity_id'],
                            'meta_option_id' => $optionId,
                            'meta_value' => Str::title(trim($value)),
                            ]);
                            DB::table('meta')->insert([
                                'entity_id' => $term['entity_id'],
                                'meta_option_id' => $optionId,
                                //'meta_key' => Str::slug(trim($gvalue['key']), '_'),
                                'meta_value' => Str::title(trim($value)),
                                'meta_group' => trim($key),
                                'type' => $type,
                                ]);
                  }
              } else {
    
                  DB::table('meta_varchar')->insert([
                      'entity_id' => $term['entity_id'],
                      'meta_option_id' => $optionId,
                      //'meta_key' => Str::slug(trim($gvalue['key']), '_'),
                      'meta_value' => Str::title(trim($gvalue['value'])),
                      ]);
                      DB::table('meta')->insert([
                        'entity_id' => $term['entity_id'],
                        'meta_option_id' => $optionId,
                        //'meta_key' => Str::slug(trim($gvalue['key']), '_'),
                        'meta_value' => Str::title(trim($gvalue['value'])),
                        'meta_group' => trim($key),
                        'type' => $type,
                        ]);
              }
 
        }
      }
    return response()->json([convert(memory_get_usage()), $term]);
});

$router->get('/entity/{type_id}/{term_id}', function ($type_id, $term_id) {

    $entityIds = DB::table('term_entity')->where('term_taxonomy_id', $term_id)->get()->pluck('entity_id');
    $collection = DB::table('epeyContent')->whereIn('id', $entityIds)->limit(10)->get();

    return response()->json($collection ? $collection : 0);
});


$router->get('/term/{level_1}/{level_2}', function ($level_1, $level_2) {

    $testjson = TermVariant::selectRaw("entity.*")
    //$testjson = TermVariant::selectRaw("entity.title, entity.cat")
    //$testjson = TermVariant::selectRaw("group_concat(entity.entity_id) as ids")
    //->where('term_variant.variant_id', 645)
    ->where('term_variant.term_type_id', 15)
    ->where('terms.slug', $level_2)
    ->setJoinTerm()
    ->setJoinTermEntity()
    ->setJoinEntity()
    ->limit(10)
    ->get();
    return response()->json($testjson);
});



$router->get('/entry-type/{type_id}', function ($type_id) {

    $entity = Entity::table('entity')
        ->where('type_id', $type_id)
        ->get();

    return response()->json($entity);
});
$router->get('/entry/{entity_id}', function ($entity_id) {

    $entity = Entity::find($entity_id);

    return response()->json($entity);
});

$router->get('/last10', function () {

    $entities = Entity::limit(20)
        //->where('type_id', $type_id)
        //->where('type_id', 4)
        
        ->get();

    return response()->json($entities);
});
$router->get('/entry/{type_id}/{item_id}', function ($type_id, $item_id) {

    $entity = DB::table('entity')
        ->where('type_id', $type_id)
        ->where('entity_id', $item_id)
        ->first();

    return response()->json($entity);
});

$router->get('/terms/{type_id}', function ($type_id) {

    $termTaxonomyIds = DB::table('term_variant')->where('term_type_id', $type_id)
        ->leftJoin('terms', 'term_variant.term_id', '=', 'terms.term_id')
//->where('term_taxonomy_id', $item_id)
        ->get();
/*->map(function ($term) {
$term->termname = DB::table('terms')->where('term_id',$term->term_id)->first();
return $term;
})->toArray();*/
//->pluck('term_taxonomy_id');
    //$collection = DB::table('terms')->whereIn('term_id',$termTaxonomyIds)->limit(10)->get();

    return response()->json($termTaxonomyIds);
});

$router->get('/terms/{type_id}/{term_id}', function ($type_id, $term_id) {
    $termTaxonomyIds = DB::table('term_taxonomy')
        ->where('type_id', $type_id)
        ->where('term_taxonomy_id', $term_id)
        ->leftJoin('terms', 'term_taxonomy.term_id', '=', 'terms.term_id')
        ->get();

    return response()->json($termTaxonomyIds);
});

$router->get('/termsold/{type_id}/{term_id}/{entry_type_id}', function ($term_type_id, $term_id, $entry_type_id) {
//http://arge01.test/api/terms/15/646/2

    $termTaxonomyIds = DB::table('term_entity')
        ->leftJoin('entity', 'term_entity.entity_id', '=', 'entity.entity_id')
        ->leftJoin('term_taxonomy', 'term_entity.term_taxonomy_id', '=', 'term_taxonomy.term_taxonomy_id')
        ->leftJoin('terms', 'term_taxonomy.term_id', '=', 'terms.term_id')
        //->where('entity.type_id', $entry_type_id)
        //->where('term_entity.term_taxonomy_id', $term_id)
        ->get();

    return response()->json($termTaxonomyIds);
});



$router->get('/karsilastirma/{post_id}', 'ExampleController@getEntity');
$router->get('/karsilastirma', 'ExampleController@getEntityCollection');
$router->get('/term-entities/{type_id}/{term_id}/{entry_type_id}', 'ExampleController@getEntityAsTerm');
$router->post('/term-insert/{type_id}/{term_name}', 'ExampleController@insertTerm');
/*


*/
//http://wikiproduct.test/teknoloji-akilli-saatler-model-garmin-t15-i645.html
$router->get('/{slug}-t{type_id}-i{item_id}.html', function ($slug,$type_id,$item_id) {
    $entry_type = EntryType::findOrFail($type_id);
    $response = [$entry_type];
    if($entry_type->property_code == 'term'){
        $response[] = TermVariant::where('variant_id', $item_id)->where('term_type_id', $type_id)->first();
    } elseif ($entry_type->property_code == 'entity') {
        $response[] = Entity::find($item_id);
    }
    return response()->json([$response,$slug,$type_id,$item_id]);
});




//http://wikiproduct.test/baska-t15-i4554/sleeppeople-comfort-visco-yatak-paket-yayli-160-x-200-visco-yastik-e4-i645
$router->get('/{term_slug}-t{term_type_id}-i{term_item_id}/{entity_slug}-e{entity_type_id}-i{entity_item_id}', function ($term_slug,$term_type_id,$term_item_id,$entity_slug,$entity_type_id,$entity_item_id) {
    $entry_type = EntryType::findOrFail($term_type_id);
    $response = [$entry_type];
    if($entry_type->property_code == 'term'){
        $response[] = TermVariant::where('variant_id', $term_item_id)->with(['term'])->where('term_type_id', $term_type_id)->first();
    if (EntryType::findOrFail($entity_type_id)->property_code == 'entity') {
        $response[] = Entity::find($entity_item_id);
    }
    }
    return response()->json([$response,$term_slug,$term_type_id,$term_item_id,$entity_slug,$entity_type_id,$entity_item_id]);
});


//http://wikiproduct.test/test02/Garmin
$router->get('/test02/{slug}', function ($slug) {

    $testjson = TermVariant::selectRaw("*")
    //$testjson = TermVariant::selectRaw("entity.title, entity.cat")
    //$testjson = TermVariant::selectRaw("group_concat(entity.entity_id) as ids")
    ->where('term_variant.term_type_id', 15)
    //->where('term_variant.variant_id', 645)
    ->Where('terms.slug', $slug)
    ->setJoinTerm()
    ->setJoinTermEntity()
    ->setJoinEntity()
    ->limit(10)
    ->first();
    //->pluck('title');
    $testjson = $testjson->childrenRecursive;
    return response()->json($testjson ? $testjson : 0);
});

/*

*/
$router->get('/{slug}-fiyatlari', function ($slug) {

    $testjson = $slug;
    return response()->json($testjson ? $testjson : 0);
});

$router->get('/{slug}[/p-{product_id:[0-9]+}]', function ($slug, $product_id = 1) {

    $testjson = [$slug,$product_id,Str::slug('Askan AşğçĞç (?# $""', '_')];
    return response()->json($testjson ? $testjson : 0);
});





