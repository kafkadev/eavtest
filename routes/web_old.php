<?php
use Illuminate\Support\Facades\DB;
use App\Models\Content;
use App\Models\EntryType;
use App\Models\Entity;
use App\Models\TermEntity;
use App\Models\Term;
use App\Models\TermVariant;
use Illuminate\Support\Str;

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/insertTerm', 'ExampleController@insertTermTest');
$router->get('/termEntityTest', 'ExampleController@termEntityTest');
$router->get('/entityTermsTest', 'ExampleController@entityTermsTest');
$router->get('/tests/fts565', function () {

    $testjson = DB::table('PostSearch')->select('*')->whereRaw("PostSearch MATCH 'term_slug:Garmin'")->get();
    return response()->json($testjson ? $testjson : 0);
});
$router->get('/termTest', function () {

    return response()->json($testjson ? $testjson : 0);
});

$router->get('/entity/{type_id}/{term_id}', function ($type_id, $term_id) {

    $entityIds = DB::table('term_entity')->where('term_taxonomy_id', $term_id)->get()->pluck('entity_id');
    $collection = DB::table('epeyContent')->whereIn('id', $entityIds)->limit(10)->get();

    return response()->json($collection ? $collection : 0);
});


$router->get('/term/{level_1}/{level_2}', function ($level_1, $level_2) {

    $testjson = TermVariant::selectRaw("entity.*")
    //$testjson = TermVariant::selectRaw("entity.title, entity.cat")
    //$testjson = TermVariant::selectRaw("group_concat(entity.entity_id) as ids")
    //->where('term_variant.variant_id', 645)
    ->where('term_variant.term_type_id', 15)
    ->where('terms.slug', $level_2)
    ->setJoinTerm()
    ->setJoinTermEntity()
    ->setJoinEntity()
    ->limit(10)
    ->get();
    return response()->json($testjson);
});



$router->get('/entry-type/{type_id}', function ($type_id) {

    $entity = Entity::table('entity')
        ->where('type_id', $type_id)
        ->get();

    return response()->json($entity);
});
$router->get('/entry/{entity_id}', function ($entity_id) {

    $entity = Entity::find($entity_id);

    return response()->json($entity);
});

$router->get('/last10', function () {

    $entities = Entity::limit(20)
        //->where('type_id', $type_id)
        //->where('type_id', 4)
        
        ->get();

    return response()->json($entities);
});
$router->get('/entry/{type_id}/{item_id}', function ($type_id, $item_id) {

    $entity = DB::table('entity')
        ->where('type_id', $type_id)
        ->where('entity_id', $item_id)
        ->first();

    return response()->json($entity);
});

$router->get('/terms/{type_id}', function ($type_id) {

    $termTaxonomyIds = DB::table('term_variant')->where('term_type_id', $type_id)
        ->leftJoin('terms', 'term_variant.term_id', '=', 'terms.term_id')
//->where('term_taxonomy_id', $item_id)
        ->get();
/*->map(function ($term) {
$term->termname = DB::table('terms')->where('term_id',$term->term_id)->first();
return $term;
})->toArray();*/
//->pluck('term_taxonomy_id');
    //$collection = DB::table('terms')->whereIn('term_id',$termTaxonomyIds)->limit(10)->get();

    return response()->json($termTaxonomyIds);
});

$router->get('/terms/{type_id}/{term_id}', function ($type_id, $term_id) {
    $termTaxonomyIds = DB::table('term_taxonomy')
        ->where('type_id', $type_id)
        ->where('term_taxonomy_id', $term_id)
        ->leftJoin('terms', 'term_taxonomy.term_id', '=', 'terms.term_id')
        ->get();

    return response()->json($termTaxonomyIds);
});

$router->get('/termsold/{type_id}/{term_id}/{entry_type_id}', function ($term_type_id, $term_id, $entry_type_id) {
//http://arge01.test/api/terms/15/646/2

    $termTaxonomyIds = DB::table('term_entity')
        ->leftJoin('entity', 'term_entity.entity_id', '=', 'entity.entity_id')
        ->leftJoin('term_taxonomy', 'term_entity.term_taxonomy_id', '=', 'term_taxonomy.term_taxonomy_id')
        ->leftJoin('terms', 'term_taxonomy.term_id', '=', 'terms.term_id')
        //->where('entity.type_id', $entry_type_id)
        //->where('term_entity.term_taxonomy_id', $term_id)
        ->get();

    return response()->json($termTaxonomyIds);
});



$router->get('/karsilastirma/{post_id}', 'ExampleController@getEntity');
$router->get('/karsilastirma', 'ExampleController@getEntityCollection');
$router->get('/term-entities/{type_id}/{term_id}/{entry_type_id}', 'ExampleController@getEntityAsTerm');
$router->post('/term-insert/{type_id}/{term_name}', 'ExampleController@insertTerm');
/*


*/
//http://wikiproduct.test/teknoloji-akilli-saatler-model-garmin-t15-i645.html
$router->get('/{slug}-t{type_id}-i{item_id}.html', function ($slug,$type_id,$item_id) {
    $entry_type = EntryType::findOrFail($type_id);
    $response = [$entry_type];
    if($entry_type->property_code == 'term'){
        $response[] = TermVariant::where('variant_id', $item_id)->where('term_type_id', $type_id)->first();
    } elseif ($entry_type->property_code == 'entity') {
        $response[] = Entity::find($item_id);
    }
    return response()->json([$response,$slug,$type_id,$item_id]);
});




//http://wikiproduct.test/baska-t15-i4554/sleeppeople-comfort-visco-yatak-paket-yayli-160-x-200-visco-yastik-e4-i645
$router->get('/{term_slug}-t{term_type_id}-i{term_item_id}/{entity_slug}-e{entity_type_id}-i{entity_item_id}', function ($term_slug,$term_type_id,$term_item_id,$entity_slug,$entity_type_id,$entity_item_id) {
    $entry_type = EntryType::findOrFail($term_type_id);
    $response = [$entry_type];
    if($entry_type->property_code == 'term'){
        $response[] = TermVariant::where('variant_id', $term_item_id)->with(['term'])->where('term_type_id', $term_type_id)->first();
    if (EntryType::findOrFail($entity_type_id)->property_code == 'entity') {
        $response[] = Entity::find($entity_item_id);
    }
    }
    return response()->json([$response,$term_slug,$term_type_id,$term_item_id,$entity_slug,$entity_type_id,$entity_item_id]);
});


//http://wikiproduct.test/test02/Garmin
$router->get('/test02/{slug}', function ($slug) {

    $testjson = TermVariant::selectRaw("*")
    //$testjson = TermVariant::selectRaw("entity.title, entity.cat")
    //$testjson = TermVariant::selectRaw("group_concat(entity.entity_id) as ids")
    ->where('term_variant.term_type_id', 15)
    //->where('term_variant.variant_id', 645)
    ->Where('terms.slug', $slug)
    ->setJoinTerm()
    ->setJoinTermEntity()
    ->setJoinEntity()
    ->limit(10)
    ->first();
    //->pluck('title');
    $testjson = $testjson->childrenRecursive;
    return response()->json($testjson ? $testjson : 0);
});

/*

*/
$router->get('/{slug}-fiyatlari', function ($slug) {

    $testjson = $slug;
    return response()->json($testjson ? $testjson : 0);
});

$router->get('/{slug}[/p-{product_id:[0-9]+}]', function ($slug, $product_id = 1) {

    $testjson = [$slug,$product_id,Str::slug('Askan AşğçĞç (?# $""', '_')];
    return response()->json($testjson ? $testjson : 0);
});





