<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


$router->get('/', function () use ($router) {
    //include('alis/index.html');
 return $router->app->version();
});




$router->get('/cdn/{catName}', function ($catName) {
    $file = file_get_contents('https://cdn.cimri.io/image/480x480/x_' .$catName);
 file_put_contents('cdn/' . $catName, $file);
    return response($file)->header('Content-Type', 'image/jpeg');
});


